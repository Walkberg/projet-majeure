import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Connexion from "../views/connexion";
import Landing from "../views/landing";
import Register from "../views/register";
import Remote from "../views/remote";
import Scanning from "../views/scanning";

import { ScreenLabels } from "../constant";

const routes = [
  {
    path: ScreenLabels.CONNEXION,
    exact: true,
    main: Connexion
  },
  {
    path: ScreenLabels.REGISTER,
    exact: false,
    main: Register
  },
  {
    path: ScreenLabels.REMOTE,
    exact: false,
    main: Remote
  },
  {
    path: ScreenLabels.SCANNING,
    exact: false,
    main: Scanning
  },
  {
    path: ScreenLabels.LANDING,
    exact: true,
    main: Landing
  }
];

export default function AppRouter() {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      ))}
    </Switch>
  );
}
