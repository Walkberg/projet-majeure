function InputNetwork(playerId) {
  // Accept name and age in the constructor
  this.playerId = playerId;
  this.buttonPress = false;
  this.buttonAPress = false;
}

InputNetwork.prototype.getButtonPress = function() {
  return this.buttonPress;
};

InputNetwork.prototype.setButtonPress = function(isPress) {
  this.buttonPress = isPress;
};

module.exports = InputNetwork;
