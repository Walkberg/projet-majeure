import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";

import CustomButton from "../components/CustomButton";
const useStyles = makeStyles(theme => ({
  container: {}
}));

function App() {
  const classes = useStyles();
  const [login, setLogin] = useState({ login: "", password: "" });

  const handleChange = e => {
    setLogin({ ...login, [e.target.name]: e.target.value });
  };

  const handleSubmit = () => {
    console.log("submit");
  };

  return (
    <>
      <Typography variant="h4" component="h2" gutterBottom>
        Welcome to move it
      </Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          id="outlined-search"
          label="Search field"
          type="text"
          variant="outlined"
          name="login"
          value={login.login}
          onChange={handleChange}
        />
        <TextField
          id="outlined-search"
          label="Search field"
          type="password"
          variant="outlined"
          name="password"
          value={login.password}
          onChange={handleChange}
        />
        <CustomButton onClick={handleSubmit}>Connect</CustomButton>
      </form>
      <Typography variant="h4" component="h2" gutterBottom>
        No account yet?
      </Typography>
      <Typography variant="h4" component="h2" gutterBottom>
        Sign In
      </Typography>
      <Typography variant="h1" component="h2" gutterBottom>
        Or
      </Typography>
      <CustomButton>Play as guest</CustomButton>
    </>
  );
}

export default App;
