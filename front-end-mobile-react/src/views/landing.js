import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import QrReader from "react-qr-reader";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";
import { Container } from "@material-ui/core";
import { ScreenLabels } from "../constant";
import CustomButton from "../components/CustomButton";
import { socket } from "../config/SocketIO";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
  },
}));

function App() {
  console.log("test");
  const history = useHistory();
  const classes = useStyles();
  const [code, setCode] = useState("");
  const [isScanning, setIsScanning] = useState(false);

  const handleChange = (e) => {
    setCode(e.target.value);
  };

  const handleScanButton = () => {
    setIsScanning(!isScanning);
    console.log("scan");
  };

  const handleJoinButton = () => {
    socket.emit("JOIN_ROOM", { roomId: code });
  };

  const handleReadyButton = () => {
    socket.emit("TOGGLE_PLAYER_READY");
    history.push("/remote");
  };

  const handleSpawnButton = () => {
    //history.push(ScreenLabels.REMOTE);
    socket.emit("spawn", code);
  };

  const handleScan = (data) => {
    if (data) {
      setCode(data);
    }
  };
  const handleError = (err) => {
    console.error(err);
  };

  return (
    <Container className={classes.container}>
      <Typography variant="h4" component="h2" gutterBottom>
        To play
      </Typography>
      <CustomButton onClick={handleScanButton}>Scan</CustomButton>
      <Typography variant="h1" component="h2" gutterBottom>
        Or
      </Typography>
      {!isScanning ? (
        <form className={classes.container} onSubmit={handleJoinButton}>
          <TextField
            id="outlined-search"
            label="Search field"
            type="string"
            variant="outlined"
            name="matchMaking"
            value={code}
            onChange={handleChange}
          />
          <CustomButton onClick={handleJoinButton}>Join</CustomButton>
          <CustomButton onClick={handleReadyButton}>Ready</CustomButton>
          <CustomButton onClick={handleSpawnButton}>spawn </CustomButton>
        </form>
      ) : (
        <QrReader
          delay={300}
          onError={handleError}
          onScan={handleScan}
          style={{ width: "100%" }}
        />
      )}
    </Container>
  );
}

export default App;
