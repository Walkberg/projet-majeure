import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ReactNipple from "react-nipple";
import { socket } from "../config/SocketIO";
import Joystick from "../components/Joystick";
import GamePadButton from "../components/GamePadButton";
import KeyCode from "../constant/keyCode";
import Button from "@material-ui/core/Button";
import { Container, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
    flexGrow: 1,
  },
  mid: {
    flex: 1,
    backgroundColor: "green",
  },
  mid1: {
    flex: 1,
    backgroundColor: "blue",
  },
  button: {
    height: "333px",
    width: "100%",
  },
}));

function App() {
  console.log("vdsvovlvxf");
  const classes = useStyles();
  const [test, setTest] = useState("");
  const [coucou, setCoucou] = useState("");

  let [previousPosX, setPreviousPosX] = useState("coiucou");
  let [previousPosY, setPreviousPosY] = useState();

  const handleClick = () => {
    socket.emit("PRESS_BUTTON", true);
  };

  const setPreviousPos = (type) => {
    console.log("set previous post");
    console.log(type);
    setPreviousPosX(type);
    setTest("5fdsfdsfs");
    console.log(test);
    //console.log(previousPosX);
  };

  // onMouseDown={ handleEvent } onMouseUp={ handleEvent }

  const handleEvent = (e) => {
    if (e.type === "mousedown" || e.type === "touchstart") {
      socket.emit("PRESS_BUTTON", { id: e.target.name, value: true });
    } else {
      socket.emit("PRESS_BUTTON", { id: e.target.name, value: false });
    }
  };

  return (
    <Grid container alignItems="stretch">
      {/* <Grid item style={{ backgroundColor: "blue" }}>
        <Joystick
          previousPosX={previousPosX}
          previousPosY={previousPosY}
          setPosition={setPreviousPos}
        />
      </Grid> */}

      <Grid container alignItems="stretch">
        <Grid item xs={4}>
          <button
            className={classes.button}
            name={KeyCode.Left}
            onMouseDown={handleEvent}
            onMouseUp={handleEvent}
            onTouchStart={handleEvent}
            onTouchEnd={handleEvent}
          >
            Gauche
          </button>
        </Grid>
        <Grid item xs={4}>
          <button
            className={classes.button}
            name={KeyCode.Up}
            onMouseDown={handleEvent}
            onMouseUp={handleEvent}
            onTouchStart={handleEvent}
            onTouchEnd={handleEvent}
          >
            Up
          </button>
        </Grid>
        <Grid item xs={4}>
          <button
            className={classes.button}
            name={KeyCode.Right}
            onMouseDown={handleEvent}
            onMouseUp={handleEvent}
            onTouchStart={handleEvent}
            onTouchEnd={handleEvent}
          >
            Droite
          </button>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default App;
