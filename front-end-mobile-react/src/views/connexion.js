import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";
import { Container } from "@material-ui/core";

import { useHistory, Link } from "react-router-dom";
import { ScreenLabels } from "../constant";
import CustomButton from "../components/CustomButton";
const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
  },
}));

function App() {
  const classes = useStyles();
  const history = useHistory();
  const [login, setLogin] = useState({ login: "", password: "" });

  const handleChange = (e) => {
    setLogin({ ...login, [e.target.name]: e.target.value });
  };

  const handleSubmit = () => {
    history.push(ScreenLabels.LANDING);
  };

  const handlePlayAsGuest = () => {
    history.push(ScreenLabels.LANDING);
  };

  return (
    <Container className={classes.container}>
      <Typography variant="h4" component="h2" gutterBottom>
        Welcome to move it
      </Typography>
      <form className={classes.container} onSubmit={handleSubmit}>
        <TextField
          id="outlined-search"
          label="Search field"
          type="text"
          variant="outlined"
          name="login"
          value={login.login}
          onChange={handleChange}
        />
        <TextField
          id="outlined-search"
          label="Search field"
          type="password"
          variant="outlined"
          name="password"
          value={login.password}
          onChange={handleChange}
        />
        <CustomButton onClick={handleSubmit}>Connect</CustomButton>
      </form>
      <Typography variant="h4" component="h2" gutterBottom>
        No account yet?
      </Typography>
      <Link to="./register">
        <Typography variant="h4" component="h4" gutterBottom>
          Sign In
        </Typography>
      </Link>
      <Typography variant="h1" component="h2" gutterBottom>
        Or
      </Typography>
      <CustomButton onClick={handlePlayAsGuest}>Play as guest</CustomButton>
    </Container>
  );
}

export default App;
