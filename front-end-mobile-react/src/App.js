import React, { useState } from "react";
import "./App.css";

import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";

import Connexion from "./views/connexion";
import Landing from "./views/landing";
import Remote from "./views/remote";

import SocketIOContainer from "./container/socketIOContainer";

import CssBaseline from "@material-ui/core/CssBaseline";
import { BrowserRouter } from "react-router-dom";

import Router from "./routes";

import CustomButton from "./components/CustomButton";
const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1
  }
}));

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <CssBaseline />
      <BrowserRouter>
        <SocketIOContainer>
          <Router />
        </SocketIOContainer>
      </BrowserRouter>
    </div>
  );
}

export default App;
