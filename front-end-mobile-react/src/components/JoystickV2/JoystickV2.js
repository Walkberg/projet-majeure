import React, { useEffect, useState } from "react";
import nipplejs from "nipplejs";

const size = 200;
const triger = 0.5;

var options = {
  mode: "static",
  color: "red",
  //lockX: true,
  threshold: 50,
  size: size,
  position: { left: "70%", bottom: "50%" },
  dynamicPage: true
};

function Joystick() {
  const [previousPosX, setPreviousPosX] = useState(0);
  const [previousPosY, setPreviousPosY] = useState(0);

  useEffect(() => {
    console.log("je passe ici");
    createJoystick(options);
  }, []);

  function bindNipple(joystick) {
    joystick
      .on("start end", function(evt, data) {
        //console.log(evt.type);
        //console.log(data);
      })
      .on("move", function(evt, data) {
        const x = data.instance.frontPosition.x / (size / 2);
        const y = data.instance.frontPosition.y / (size / 2);

        const vector = { x: x, y: y };
        const vector2 = { xPrev: previousPosX, yprev: previousPosY };
        console.log(vector);
        console.log(vector2);
        console.log(Math.abs(previousPosX - x));
        console.log(Math.abs(previousPosY - y));
        setPreviousPosX(x);
        setPreviousPosY(y);
        if (
          Math.abs(previousPosX - x) < triger ||
          Math.abs(previousPosY - y) < triger
        ) {
          console.log("change");
        }
      });
  }

  const createJoystick = options => {
    let joystick = nipplejs.create(options);
    bindNipple(joystick);
  };

  return <div id="zone_joystick" style={{ backgroundColor: "red" }} />;
}

export default Joystick;
