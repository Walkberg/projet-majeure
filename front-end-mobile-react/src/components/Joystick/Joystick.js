import React, { useEffect, useState } from "react";
import nipplejs from "nipplejs";

const SIZE = 200;
const TRIGER = 0.5;

var options = {
  mode: "static",
  color: "red",
  threshold: 50,
  size: SIZE,
  position: { left: "70%", bottom: "50%" },
  dynamicPage: true,
};

function Joystick({ setPosition }) {
  const [previousPosX, setPreviousPosX] = useState(0);
  const [previousPosY, setPreviousPosY] = useState(0);

  useEffect(() => {
    createJoystick(options);
  }, []);

  function bindNipple(
    joystick,
    previousPosX,
    previousPosY,
    setPreviousPosX,
    setPreviousPosY
  ) {
    joystick
      .on("start end", function (evt, data) {})
      .on("move", function (evt, data) {
        const x = data.instance.frontPosition.x / (SIZE / 2);
        const y = data.instance.frontPosition.y / (SIZE / 2);

        console.log(previousPosX);
        console.log(previousPosY);

        const vector = { x: x, y: y };
        const prevVector = { xPrev: previousPosX, yprev: previousPosY };

        //(x) => setPreviousPosX(x);
        if (
          Math.abs(previousPosX - x) < TRIGER ||
          Math.abs(previousPosY - y) < TRIGER
        ) {
          console.log("change");
        }
      });
  }

  const createJoystick = (options) => {
    let joystick = nipplejs.create(options);
    bindNipple(
      joystick,
      previousPosX,
      previousPosY,
      setPreviousPosX,
      setPreviousPosY
    );
  };

  return (
    <div
      className="zone_joystick"
      style={{ display: "flex", backgroundColor: "red" }}
    >
      rtertre
    </div>
  );
}

export default Joystick;
