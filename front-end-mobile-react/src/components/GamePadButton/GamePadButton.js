import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1)
    //transform: [{ rotate: "90deg" }]
  },
  typo: {
    //transform: [{ rotate: "90deg" }]
  }
}));

export default function GamePadButton({ onClick, color, label }) {
  const classes = useStyles();

  return (
    <Fab
      onClick={onClick}
      color="primary"
      style={{ backgroundColor: color }}
      aria-label="add"
      className={classes.fab}
    >
      <Typography
        style={{ writingMode: "vertical-rl" }}
        className={classes.typo}
      >
        {label}
      </Typography>
    </Fab>
  );
}
