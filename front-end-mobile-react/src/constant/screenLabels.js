const ScreenLabels = {
  CONNEXION: "/connexion",
  REGISTER: "/register",
  LANDING: "/",
  REMOTE: "/remote",
  SCANNING: "/scanning"
};

export default ScreenLabels;
