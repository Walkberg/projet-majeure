const KeyCode = {
  Right: "right",
  Left: "left",
  Up: "up",
  Down: "down"
};

export default KeyCode;
