import React, { useEffect } from "react";
import { socket } from "../config/SocketIO";
import { useHistory } from "react-router";

function initSocketIO(history) {
  // socket.on("TOOGLE_PLAYER_READY", redirect(history));
}

export default function SocketIOContainer({ children }) {
  const history = useHistory();
  useEffect(() => {
    initSocketIO(history);
  }, []);

  return <div>{children}</div>;
}
