import React, { useState } from "react";

import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Group from "@material-ui/icons/Group";
import Notification from "@material-ui/icons/NotificationImportant";
import Avatar from "@material-ui/core/Avatar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";

import MenuAvatar from "../MenuAvatar";
import MenuFriend from "../MenuFriend";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(1)
  },
  title: {
    alignItems: "center",
    justifyContent: "center",
    color: "#10BBB4",
    flexGrow: 1
  },
  avatar: {
    color: "#fff",
    backgroundColor: "#10BBB4"
  },
  appbar: {
    backgroundColor: "#fff"
  }
}));

export default function ButtonAppBar() {
  const history = useHistory();
  const classes = useStyles();
  const [openMenu, setOpenMenu] = useState(false);
  const [openFriendMenu, setOpenFriendMenu] = useState(false);
  const [openAlertMenu, setOpeAlertMenu] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleMenuFriendOpen = event => {
    setAnchorEl(event.currentTarget);
    setOpenFriendMenu(true);
  };

  const handleMenuFriendClose = () => {
    setAnchorEl(null);
    setOpenFriendMenu(false);
  };

  const handleMenuOpen = event => {
    setAnchorEl(event.currentTarget);
    setOpenMenu(true);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setOpenMenu(false);
  };

  const goToLanding = () => {
    history.push("/");
  };

  const renderFriendMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      //id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={openFriendMenu}
      onClose={() => handleMenuFriendClose()}
    >
      <MenuItem onClick={handleMenuFriendClose}>Setting</MenuItem>
      <MenuItem onClick={handleMenuFriendClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuFriendClose}>Log out</MenuItem>
    </Menu>
  );

  return (
    <>
      <AppBar position="static">
        <Toolbar className={classes.appbar}>
          <div className={classes.title}></div>
          <Typography variant="h4" className={classes.title}>
            <Box fontWeight="fontWeightBold" m={1} onClick={goToLanding}>
              Move IT
            </Box>
          </Typography>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="textSecondary"
            aria-label="menu"
          >
            <Notification />
          </IconButton>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="textSecondary"
            aria-label="menu"
            onClick={e => handleMenuFriendOpen(e)}
          >
            <Group />
          </IconButton>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="textSecondary"
            aria-label="menu"
            onClick={e => handleMenuOpen(e)}
          >
            <Avatar className={classes.avatar}>OP</Avatar>
          </IconButton>
        </Toolbar>
      </AppBar>
      <MenuAvatar
        anchorEl={anchorEl}
        onClose={handleMenuClose}
        open={openMenu}
      />
      <MenuFriend
        anchorEl={anchorEl}
        onClose={handleMenuFriendClose}
        open={openFriendMenu}
      />
    </>
  );
}
