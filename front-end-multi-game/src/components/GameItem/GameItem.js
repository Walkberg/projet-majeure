import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MoreHoriz from "@material-ui/icons/MoreHoriz";
import PlayArrow from "@material-ui/icons/PlayArrow";

import { useIntl } from "react-intl";

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: 345,
    "&:hover": {
      border: "2px solid green",
    },
  },
  media: {
    height: "100%",

    paddingTop: "100%", // 16:9
  },
  action1: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  action2: {
    display: "flex",
    alignItems: "center",
    justifyContent: "left",
  },
}));

export default function GameItem({ data, onClickPlay, onClickDetail }) {
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Card className={classes.card} onClick={() => onClickPlay(data)}>
      <CardMedia
        className={classes.media}
        image={data.imageUrl}
        title="game picture"
      />
      <div className={classes.action1}>
        <Typography variant="body2" color="textSecondary" component="p">
          {data.title}
        </Typography>
        <IconButton
          aria-label="add to favorites"
          onClick={() => onClickDetail(data)}
        >
          <MoreHoriz />
        </IconButton>
      </div>
      <div className={classes.action2}>
        <IconButton aria-label="add to favorites">
          <PlayArrow />
        </IconButton>
        <Typography variant="body2" color="textSecondary" component="p">
          {intl.formatMessage({
            id: "launch",
            defaultMessage: "Launch",
          })}
        </Typography>
      </div>
    </Card>
  );
}
