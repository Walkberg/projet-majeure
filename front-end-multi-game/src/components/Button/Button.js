import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

// The `withStyles()` higher-order component is injecting a `classes`
// prop that is used by the `Button` component.
const StyledButton = withStyles({
  root: {
    background: "linear-gradient(90deg, #10BBB4 30%, #27D585 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 48,
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)"
  },
  label: {
    textTransform: "capitalize"
  }
})(Button);

export default function CustomButton({
  children,
  onClick,
  fullWidth,
  ...rest
}) {
  return (
    <StyledButton onClick={onClick} fullWidth {...rest}>
      {children}
    </StyledButton>
  );
}
