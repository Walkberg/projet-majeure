import React, { useState, useEffect } from "react";
import QRCode from "qrcode.react";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  qrCodeContainer: {
    position: " absolute",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    right: "2%",
    bottom: "3%",
    padding: theme.spacing(1),
  },
}));

const Qrcode = ({ position }) => {
  const classes = useStyles();

  const { roomId } = useSelector((state) => ({
    roomId: state.matchMaking.room,
  }));

  return (
    <Paper className={classes.qrCodeContainer}>
      <QRCode value={roomId} />
      {roomId}
    </Paper>
  );
};

export default Qrcode;
