import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import { Typography, Avatar } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1)
    //transform: [{ rotate: "90deg" }]
  }
}));

export default function GamePadButton({ color, label }) {
  const classes = useStyles();

  return (
    <Avatar
      color="primary"
      style={{ backgroundColor: color }}
      aria-label="add"
      className={classes.fab}
    >
      <Typography>{label}</Typography>
    </Avatar>
  );
}
