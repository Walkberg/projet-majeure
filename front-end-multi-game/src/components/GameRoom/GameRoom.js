import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Paper } from "@material-ui/core";
import Smartphone from "../Smartphone";

const useStyles = makeStyles(theme => ({
  container: {
    //margin: theme.spacing(3),
    padding: theme.spacing(3)
  }
}));

export default function MatchMaking({ players }) {
  const classes = useStyles();

  return (
    <Grid container justify="center" alignItems="center">
      <Grid item justify="center" alignItems="stretch" xs>
        <Paper className={classes.container}>
          {players.length <= 0 ? (
            <Typography
              align="center"
              variant="h5"
              color="textSecondary"
              component="p"
            >
              Scan or enter the code above in the mobile App !
            </Typography>
          ) : (
            <>
              <Typography
                align="center"
                variant="h5"
                color="textSecondary"
                component="p"
              >
                Press A when ready !
              </Typography>
              <Grid item container justify="center" alignItems="center">
                {players.map((player, index) => (
                  <Grid
                    item
                    direction="column"
                    justify="center"
                    alignItems="center"
                  >
                    <Smartphone
                      color={player.isReady ? player.color : "#777"}
                    />
                    <Typography
                      align="center"
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {player.name}
                    </Typography>
                  </Grid>
                ))}
              </Grid>
            </>
          )}
        </Paper>
      </Grid>
    </Grid>
  );
}
