import React, { useState } from "react";

import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";

import { IntlContext } from "../../config/IntlProviderWrapper";

const Language = [
  { value: "en", name: "English" },
  { value: "fr", name: "French" }
];

function LanguageButton() {
  const [language, setLanguage] = useState("en");

  const handleChange = (e, toogleLanguage) => {
    const { value } = e.target;
    setLanguage(value);
    toogleLanguage(value);
  };

  return (
    <IntlContext.Consumer>
      {({ toogleLanguage, locale }) => (
        <TextField
          onChange={e => handleChange(e, toogleLanguage)}
          value={locale}
          disabled={false}
          select
        >
          {Language.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.name}
            </MenuItem>
          ))}
        </TextField>
      )}
    </IntlContext.Consumer>
  );
}

export default LanguageButton;
