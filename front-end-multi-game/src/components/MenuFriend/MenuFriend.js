import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import PersonIcon from "@material-ui/icons/Person";
import LogOutIcon from "@material-ui/icons/PowerSettingsNew";
import SettingIcon from "@material-ui/icons/Settings";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import CircleIcon from "@material-ui/icons/Lens";

import Button from "../Button";
import { useSelector } from "react-redux";
//import { playerOnline } from "../../helpers/playerOnline";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  listItem: {
    paddingTop: "2px",
    paddingBottom: "2px"
  },
  button: {}
}));

export default function MenuFriend({ anchorEl, open, onClose }) {
  const [expandFriend, setExpandFriend] = useState(true);
  const classes = useStyles();

  const { players, friendsList, room } = useSelector(state => ({
    players: state.matchMaking.players,
    friendsList: state.friend.friendsList,
    room: state.matchMaking.room
  }));

  const nbFriendOnline = friendsList.filter(user => user.online === true)
    .length;
  const nbFriend = friendsList.length;

  const handleClick = () => {
    setExpandFriend(!expandFriend);
  };
  return (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={open}
      onClose={() => onClose()}
    >
      <Button className={classes.button}> Add friend </Button>
      <Typography variant="inherit">{`In Room : ${room}`}</Typography>
      {players.length > 0 ? (
        players.map(player => (
          <ListItem button className={classes.listItem}>
            <Typography variant="inherit">{player.username}</Typography>
          </ListItem>
        ))
      ) : (
        <ListItem>
          <Typography variant="inherit">No player in room</Typography>
        </ListItem>
      )}

      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <PersonIcon />
        </ListItemIcon>
        <ListItemText primary={`Online (${nbFriendOnline}/${nbFriend})`} />
        {expandFriend ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={expandFriend} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {friendsList.map(player => (
            <ListItem button className={classes.listItem}>
              <ListItemIcon>
                <CircleIcon
                  style={{
                    fontSize: 12,
                    color: player.online ? "#27D585" : "#FF4765"
                  }}
                />
              </ListItemIcon>
              <ListItemText primary={player.username} />
            </ListItem>
          ))}
        </List>
      </Collapse>
    </Menu>
  );
}
