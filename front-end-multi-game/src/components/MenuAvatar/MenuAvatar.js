import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import PersonIcon from "@material-ui/icons/Person";
import LogOutIcon from "@material-ui/icons/PowerSettingsNew";
import SettingIcon from "@material-ui/icons/Settings";

import { Link, useHistory, useLocation } from "react-router-dom";
import ScreenLabels from "../../constants/screenLabels";
import { disconnect } from "../../utils/connexion";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(1)
  },
  title: {
    alignItems: "center",
    justifyContent: "center",
    color: "#777",
    flexGrow: 1
  },
  avatar: {
    color: "#fff",
    backgroundColor: "#10BBB4"
  },
  appbar: {
    backgroundColor: "#fff"
  }
}));

export default function MenuAvatar({ anchorEl, open, onClose }) {
  const history = useHistory();
  const location = useLocation();

  const handleLogOut = () => {
    disconnect();
    history.push(ScreenLabels.CONNEXION);
  };

  const redirect = link => ({
    pathname: link,
    state: { background: location }
  });

  return (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      //id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={open}
      onClose={() => onClose()}
    >
      <MenuItem component={Link} to={redirect(ScreenLabels.SETTING)}>
        <ListItemIcon>
          <SettingIcon fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Setting</Typography>
      </MenuItem>
      <MenuItem component={Link} to={redirect(ScreenLabels.PROFILE)}>
        <ListItemIcon>
          <PersonIcon fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Profile</Typography>
      </MenuItem>
      <MenuItem onClick={handleLogOut}>
        <ListItemIcon>
          <LogOutIcon fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Log out</Typography>
      </MenuItem>
    </Menu>
  );
}
