import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { gameControls } from "../../helpers/gameControls";
import Grid from "@material-ui/core/Grid";

import Skeleton from "@material-ui/lab/Skeleton";

import GamePadButton from "../GamePadButton";
import { useParams } from "react-router";
import { useSelector } from "react-redux";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  separation: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  child: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  item: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row"
  }
}));

export default function GameRule({ rule, isLoading }) {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Typography variant="h4" color="textSecondary" component="h4">
        Game rules
      </Typography>
      <Grid container justify="flex-end" alignItems="stretch" direction="row">
        <Grid
          item
          container
          alignItems="center"
          justify="flex-start"
          direction="column"
          xs={6}
        >
          <Typography variant="h5" color="textSecondary" component="h5">
            Rule
          </Typography>
          {isLoading ? (
            <Grid item>
              <Skeleton width="250px" />
              <Skeleton width="200px" />
              <Skeleton width="250px" />
              <Skeleton width="200px" />
            </Grid>
          ) : (
            <Typography variant="body2" color="textSecondary" component="p">
              {rule}
            </Typography>
          )}
        </Grid>
        <Grid
          item
          container
          alignItems="center"
          justify="flex-start"
          direction="column"
          xs={6}
        >
          <Typography variant="h5" color="textSecondary" component="h5">
            Control
          </Typography>
          <Grid container item justify="center" direction="row">
            {gameControls.map(control => (
              <Grid
                container
                item
                direction="row"
                alignItems="center"
                justify="flex-start"
              >
                <GamePadButton label={control.button} color={control.color} />
                <Typography variant="body2" color="textSecondary" component="p">
                  {control.action}
                </Typography>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
