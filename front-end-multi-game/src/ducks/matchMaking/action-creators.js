import {
  SET_CURRENT_ROOM,
  ADD_USER_TO_ROOM,
  SET_USER_READY,
  REMOVE_USER_FROM_ROOM
} from "./action-types";

export function setCurrentRoom(room) {
  console.log("je passe ici");
  return {
    type: SET_CURRENT_ROOM,
    room
  };
}

export function AddPlayerToRoom(player) {
  console.log("je passe ici");
  return {
    type: ADD_USER_TO_ROOM,
    player
  };
}
export function removePlayerFromRoom(playerId) {
  console.log("je passe ici");
  return {
    type: REMOVE_USER_FROM_ROOM,
    playerId
  };
}

export function setPlayerReady(client) {
  return {
    type: SET_USER_READY,
    client
  };
}
