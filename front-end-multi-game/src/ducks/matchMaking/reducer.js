import {
  SET_CURRENT_ROOM,
  ADD_USER_TO_ROOM,
  SET_USER_READY,
  REMOVE_USER_FROM_ROOM,
} from "./action-types.js";

const initialState = {
  room: "",
  players: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_CURRENT_ROOM:
      return {
        ...state,
        room: action.room,
      };
    case ADD_USER_TO_ROOM:
      return {
        ...state,
        players: [...state.players, action.player],
      };
    case SET_USER_READY:
      const index = state.players.findIndex(
        (player) => player.id === action.client.id
      );
      console.log(state.players);
      let updatedPlayers = state.players;
      console.log(updatedPlayers);
      updatedPlayers[index] = action.client;
      return {
        ...state,
        players: updatedPlayers,
      };

    case REMOVE_USER_FROM_ROOM:
      const newPlayers = state.players.filter(
        (player) => player.id !== action.playerId
      );
      return { ...state, players: newPlayers };
    default:
      return state;
  }
};
