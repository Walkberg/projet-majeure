import { SET_CURRENT_THEME } from "./action";
import { getThemeType } from "../../theme/themeUtils";

const initialState = { theme: { type: getThemeType() } };

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_CURRENT_THEME:
      return { ...state, theme: action.theme };
    default:
      return state;
  }
};
