import { setThemeType } from "../../theme/themeUtils";
export const SET_CURRENT_THEME = "SET_CURRENT_THEME";

export function setCurrentTheme(theme) {
  setThemeType(theme.type);
  return {
    type: SET_CURRENT_THEME,
    theme,
  };
}
