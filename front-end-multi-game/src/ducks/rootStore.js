import { createStore } from "redux";
import { combineReducers } from "redux";

import { authReducer } from "./auth";
import { matchmakingReducer } from "./matchMaking";
import { friendReducer } from "./friend";
import { gameReducer } from "./game";
import { themeReducer } from "./theme";

const combineReducer = combineReducers({
  matchMaking: matchmakingReducer,
  friend: friendReducer,
  theme: themeReducer,
  auth: authReducer,
  game: gameReducer,
});

//const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(combineReducer);

export default store;
