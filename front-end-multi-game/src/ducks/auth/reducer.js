import { SET_CURRENT_USER, DELETE_CURRENT_USER } from "./action-types.js";

const initialState = {
  isAuthenticated: false,
  user: {}
};

export default (state = initialState, action = {}) => {
  console.log(action.type);
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: true,
        user: action.user
      };
    case DELETE_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: false,
        user: action.user
      };
    default:
      return state;
  }
};
