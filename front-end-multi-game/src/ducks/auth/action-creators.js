import {
  SET_CURRENT_USER,
  DELETE_CURRENT_USER,
  SET_CURRENT_USER_ROLE
} from "./action-types";
import { ACCESS_TOKEN } from "../../constants";

export function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    user
  };
}

export function deleteCurrentUser() {
  return {
    type: DELETE_CURRENT_USER
  };
}
