import { LOAD_FRIEND } from "./action-types.js";
import { playerOnline } from "../../helpers/playerOnline";

const initialState = { friendsList: playerOnline };

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LOAD_FRIEND:
      return {
        ...state,
        friends: action.friends
      };

    default:
      return state;
  }
};
