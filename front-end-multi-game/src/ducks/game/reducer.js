import { SET_CURRENT_GAME, SET_GAME_LIST } from "./action-types.js";

const initialState = {
  currentGame: {},
  gamesList: []
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_CURRENT_GAME:
      return {
        ...state,
        currentGame: action.currentGame
      };
    case SET_GAME_LIST:
      return {
        ...state,
        gamesList: action.gamesList
      };
    default:
      return state;
  }
};
