export const gameControls = [
  {
    button: "A",
    color: "#11f",
    action: "Jump"
  },
  {
    button: "B",
    color: "#1ff",
    action: "Shot"
  },
  {
    button: "J",
    color: "#f1f",
    action: "Move"
  }
];
