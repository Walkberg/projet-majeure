import React, { useState } from "react";
import { IntlProvider } from "react-intl";

import enTranslation from "../i18n/locales/en.json";
import frTranslation from "../i18n/locales/fr.json";

const Context = React.createContext();

function IntlProviderWrapper({ children }) {
  const [locale, setLocale] = useState("en");
  const [messages, setMessages] = useState(enTranslation);

  const toogleLanguage = (language) => {
    setLocale(language);
    switch (language) {
      case "en":
        setMessages(enTranslation);
        break;
      case "fr":
        setMessages(frTranslation);
        break;
      default:
        setMessages(enTranslation);
    }
  };

  return (
    <Context.Provider value={{ toogleLanguage, locale }}>
      <IntlProvider
        key={locale}
        locale={locale}
        messages={messages}
        defaultLocale="en"
      >
        {children}
      </IntlProvider>
    </Context.Provider>
  );
}

export { IntlProviderWrapper, Context as IntlContext };
