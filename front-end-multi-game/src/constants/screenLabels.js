const ScreenLabels = {
  CONNEXION: "/connexion",
  TEST: "/test",
  RESET_PASSWORD: "/reset/:token",
  REGISTER: "/register",
  LANDING: "/",
  MATCHMAKING: "/game/:gameId/matchmaking",
  GAME: "/game/:gameId/",
  SETTING: "/setting",
  PROFILE: "/profile",
  GAME_DETAIL: "/game/:gameId/detail"
};

export default ScreenLabels;
