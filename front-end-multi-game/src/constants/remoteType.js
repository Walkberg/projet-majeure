const RemoteType = {
  Joystick: "joystick",
  Accelerometer: "accelerometer"
};

export default RemoteType;
