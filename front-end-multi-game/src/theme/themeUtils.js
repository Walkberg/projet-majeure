const defaultThemeType = "light";
const THEME_STORAGE = "THEME";

export const isLightTheme = () => {
  if (getThemeType() === "light") return true;
  return false;
};

export const getThemeType = () => {
  const storage = localStorage.getItem(THEME_STORAGE);
  const theme = storage ? storage : defaultThemeType;
  return theme;
};

export const setThemeType = (type) => {
  localStorage.setItem(THEME_STORAGE, type);
};
