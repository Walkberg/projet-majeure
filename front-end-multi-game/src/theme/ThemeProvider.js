import React from "react";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { useSelector } from "react-redux";

const AppThemeProvider = ({ children }) => {
  const { palette } = useSelector((state) => ({
    palette: state.theme.theme,
  }));
  const theme = createMuiTheme({ palette });

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default AppThemeProvider;
