import React from "react";
import Header from "../components/Header";
import Container from "@material-ui/core/Container";
import Qrcode from "../components/Qrcode";

const AppLayout = ({ children, ...rest }) => {
  return (
    <>
      <Header />
      <main>
        <Container>{children}</Container>
        <Qrcode position="" />
      </main>
    </>
  );
};

export default AppLayout;
