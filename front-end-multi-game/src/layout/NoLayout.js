import React from "react";

const AppLayout = ({ children, ...rest }) => {
  return (
    <main
      style={{
        flex: 1,
        height: "100%",
        width: "100%",
      }}
    >
      {children}
    </main>
  );
};

export default AppLayout;
