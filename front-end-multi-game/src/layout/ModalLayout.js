import React from "react";
import Modal from "@material-ui/core/Modal";

import { useHistory } from "react-router-dom";

const ModalLayout = ({ children, ...rest }) => {
  let history = useHistory();

  const handleClose = () => {
    history.goBack();
  };

  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={true}
      onClose={handleClose}
    >
      {children}
    </Modal>
  );
};

export default ModalLayout;
