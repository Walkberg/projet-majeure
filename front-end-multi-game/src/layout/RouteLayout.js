import React from "react";
import { Route, Redirect, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

const RouteLayout = ({
  component: Component,
  layout: Layout,
  props,
  ...rest
}) => {
  //const isAuthenticated = true;
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  return (
    <Route
      {...rest}
      render={matchProps =>
        isAuthenticated || !rest.protected ? (
          <Layout>
            <Component {...matchProps} />
          </Layout>
        ) : (
          <Redirect
            to={{
              pathname: "/connexion"
            }}
          />
        )
      }
    />
  );
};

export default RouteLayout;
