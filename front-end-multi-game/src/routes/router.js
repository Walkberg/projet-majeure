import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation
} from "react-router-dom";

import Connexion from "../views/connexion";
import ResetPassword from "../views/resetPassword";
import Landing from "../views/landing";
import Register from "../views/register";
import Setting from "../views/setting";
import Matchmaking from "../views/matchMaking";
import Game from "../views/game";
import Profile from "../views/profile";
import Test from "../views/test";
import PageNotFound from "../views/404NotFound";

import RouteLayout from "../layout/RouteLayout";

import { ScreenLabels } from "../constants";

import AppLayout from "../layout/AppLayout";
import NoLayout from "../layout/NoLayout";
import ModalLayout from "../layout/ModalLayout";

const routes = [
  {
    path: ScreenLabels.CONNEXION,
    exact: true,
    main: Connexion,
    layout: NoLayout,
    protected: false
  },
  {
    path: ScreenLabels.TEST,
    exact: true,
    main: Test,
    layout: NoLayout,
    protected: false
  },
  {
    path: ScreenLabels.RESET_PASSWORD,
    exact: true,
    main: ResetPassword,
    layout: NoLayout,
    protected: false
  },
  {
    path: ScreenLabels.REGISTER,
    exact: false,
    main: Register,
    layout: NoLayout,
    protected: false
  },
  {
    path: ScreenLabels.MATCHMAKING,
    exact: false,
    main: Matchmaking,
    layout: AppLayout,
    protected: true
  },
  {
    path: ScreenLabels.GAME,
    exact: false,
    main: Game,
    layout: NoLayout,
    protected: true
  },
  {
    path: ScreenLabels.LANDING,
    exact: true,
    main: Landing,
    layout: AppLayout,
    protected: true
  },
  {
    path: ScreenLabels.PAGE_NOT_FOUND,
    exact: false,
    main: PageNotFound,
    layout: NoLayout,
    protected: false
  }
];

const routesModal = [
  {
    path: ScreenLabels.GAME_DETAIL,
    exact: false,
    main: Setting,
    protected: true
  },
  {
    path: ScreenLabels.SETTING,
    exact: false,
    main: Setting,
    protected: true
  },
  {
    path: ScreenLabels.PROFILE,
    exact: false,
    main: Profile,
    protected: true
  }
];

export default function AppRouter() {
  let location = useLocation();
  let background = location.state && location.state.background;

  return (
    <>
      <Switch location={background || location}>
        {routes.map((route, index) => (
          <RouteLayout
            protected={route.protected}
            key={index}
            path={route.path}
            exact={route.exact}
            layout={route.layout}
            component={route.main}
          />
        ))}
      </Switch>
      {background &&
        routesModal.map((route, index) => (
          <RouteLayout
            protected={route.protected}
            key={index}
            path={route.path}
            exact={route.exact}
            layout={ModalLayout}
            component={route.main}
          />
        ))}
    </>
  );
}
