import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "../components/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withRouter, useHistory } from "react-router-dom";
import { ScreenLabels } from "../constants";
import { useIntl } from "react-intl";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    padding: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  signUp: {
    margin: theme.spacing(2)
  }
}));

const initialValue = {
  username: "",
  email: "",
  password: "",
  confirmedPassword: ""
};

export default function SignIn() {
  const classes = useStyles();
  const history = useHistory();
  const intl = useIntl();

  const [connexion, setConnexion] = useState(initialValue);

  const handleSubmit = e => {
    history.push(ScreenLabels.CONNEXION);
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setConnexion({ ...connexion, [name]: value });
  };

  const handleSignIn = () => {
    history.push(ScreenLabels.CONNEXION);
  };

  return (
    <Container component="main" maxWidth="xs">
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            onChange={handleChange}
            autoComplete="current-password"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="confirmedPassword"
            label="Password"
            type="password"
            id="password"
            onChange={handleChange}
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button type="submit" onClick={handleSubmit} fullWidth>
            Sign Up
          </Button>
        </form>
        <Grid className={classes.signUp} container xs={12}>
          <Button onClick={handleSignIn} fullWidth>
            Sign In
          </Button>
        </Grid>
      </Paper>
    </Container>
  );
}
