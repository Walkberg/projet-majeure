import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import BackIcon from "@material-ui/icons/ArrowBack";
import QRCode from "qrcode.react";
import GameRule from "../components/GameRule";
import GameRoom from "../components/GameRoom";
import { makeStyles } from "@material-ui/core/styles";
import Button from "../components/Button";
import { Link, useHistory, useParams } from "react-router-dom";
import { Grid, withStyles } from "@material-ui/core";
import { useSelector, connect, useStore } from "react-redux";
import { socket } from "../config/SocketIO";
import { ScreenLabels } from "../constants";
import { setCurrentGame } from "../ducks/game";
import { useDispatch } from "react-redux";

import RemoteType from "../constants/remoteType";

import { getGameByName } from "../api/backend-node";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
}));

export default function MatchMaking() {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();

  const { gameId } = useParams();
  const { roomId, players, currentGame } = useSelector((state) => ({
    roomId: state.matchMaking.room,
    players: state.matchMaking.players,
    currentGame: state.game.currentGame,
  }));

  const gameUrl = ScreenLabels.GAME.replace(":gameId", gameId).concat(
    `?room=${roomId}`
  );

  const goBack = () => {
    history.push(ScreenLabels.LANDING);
  };

  useEffect(() => {
    // load data from param
    if (
      Object.keys(currentGame).length === 0 &&
      currentGame.constructor === Object
    ) {
      setIsLoading(true);
      getGameByName(gameId)
        .then((data) => {
          dispatch(setCurrentGame(data));
          setIsLoading(false);
        })
        .catch(console.log("erreur"));
    } else {
      setIsLoading(false);
    }
  }, []);

  return (
    <div className={classes.container}>
      <Grid container justify="flex-start">
        <IconButton onClick={goBack}>
          <BackIcon />
        </IconButton>
      </Grid>
      <Grid container direction="column" alignItems="center" justify="center">
        <Typography variant="body2" color="textSecondary" component="p">
          Scan or enter the code
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {roomId}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          In the mobile app or the web app
        </Typography>
        <QRCode value={roomId} />
      </Grid>
      <GameRule rule={currentGame.rule} isLoading={isLoading} />
      <GameRoom players={players} />
      <Link to={gameUrl}>
        <Button style={{ marginTop: "10px" }}> PLAY</Button>
      </Link>

      {/* <Button onClick={handleChangeMannetter} style={{ marginTop: "10px" }}>
        Change Maneette
      </Button> */}
    </div>
  );
}
