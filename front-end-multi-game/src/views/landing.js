import React, { useEffect, useState } from "react";

import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

import GameItem from "../components/GameItem";
import { setGamesList, setCurrentGame } from "../ducks/game";

import { useIntl } from "react-intl";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";

import { getAllGames } from "../api/backend-node";

export default function Landing() {
  const intl = useIntl();
  const dispatch = useDispatch();
  const history = useHistory();

  const gamesList = useSelector(state => state.game.gamesList);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    if (gamesList.length === 0) {
      getAllGames()
        .then(data => {
          setIsLoading(false);
          dispatch(setGamesList(data));
        })
        .catch(error => {
          setIsLoading(true);
        });
    }
    setIsLoading(false);
  }, []);

  const handlePlay = data => {
    console.log(data);
    dispatch(setCurrentGame(data));
    history.push(`/game/${data.title}/matchmaking`);
  };

  const handleDetail = data => {
    history.push(`/game/${data.title}/detail`);
  };

  return (
    <Grid style={{ marginTop: "50px" }} container justify="left" spacing={2}>
      {!isLoading ? (
        gamesList.map((game, index) => (
          <Grid key={index} item md={2} xs={12}>
            <GameItem
              data={game}
              onClickDetail={handleDetail}
              onClickPlay={handlePlay}
            />
          </Grid>
        ))
      ) : (
        <CircularProgress />
      )}
    </Grid>
  );
}
