import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import Switch from "@material-ui/core/Switch";
import Slider from "@material-ui/core/Slider";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useIntl } from "react-intl";
import Tooltip from "@material-ui/core/Tooltip";

import { setCurrentTheme } from "../ducks/theme";

import { useDispatch } from "react-redux";

import Button from "../components/Button";

import LanguageButton from "../components/LanguageButton";
import { isLightTheme } from "../theme/themeUtils";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    padding: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
  },
}));

function ValueLabelComponent({ children, open, value }) {
  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

const lightTheme = {
  type: "light",
};

const darkTheme = {
  type: "dark",
};

export default function SignIn() {
  const classes = useStyles();
  const intl = useIntl();
  const dispatch = useDispatch();

  const [checked, setChecked] = useState(isLightTheme());

  const handleChangeTheme = (e) => {
    const check = e.target.checked;
    setChecked(check);
    dispatch(setCurrentTheme(check ? lightTheme : darkTheme));
  };

  return (
    <Container component="main" maxWidth="xs">
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h5" align="left">
          {intl.formatMessage({
            id: "setting.profile",
            defaultMessage: "undefined",
          })}
        </Typography>
        <Button>Profile</Button>
        <Typography component="h1" variant="h5" align="left">
          {intl.formatMessage({
            id: "setting.language",
            defaultMessage: "undefined",
          })}
        </Typography>
        <LanguageButton />
        <Typography component="h1" variant="h5" align="left">
          {intl.formatMessage({
            id: "setting.theme",
            defaultMessage: "undefined",
          })}
        </Typography>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="body1" align="left">
            {intl.formatMessage({
              id: "setting.darkTheme",
              defaultMessage: "undefined",
            })}
          </Typography>
          <Switch
            defaultChecked
            checked={checked}
            color="default"
            inputProps={{ "aria-label": "checkbox with default color" }}
            onChange={handleChangeTheme}
          />
        </div>

        <Typography component="h1" variant="h5" align="left">
          {intl.formatMessage({
            id: "setting.sound",
            defaultMessage: "undefined",
          })}
        </Typography>
        <Typography component="h1" variant="body2" align="left">
          {intl.formatMessage({
            id: "setting.music",
            defaultMessage: "undefined",
          })}
        </Typography>
        <Slider
          ValueLabelComponent={ValueLabelComponent}
          aria-label="custom thumb label"
          defaultValue={20}
        />
        <Typography component="h1" variant="body2" align="left">
          {intl.formatMessage({
            id: "setting.soundEffect",
            defaultMessage: "undefined",
          })}
        </Typography>
        <Slider
          ValueLabelComponent={ValueLabelComponent}
          aria-label="custom thumb label"
          defaultValue={20}
        />
      </Paper>
    </Container>
  );
}
