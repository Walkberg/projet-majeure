import React, { useState } from "react";
import { withRouter, useHistory } from "react-router-dom";
import { Container } from "@material-ui/core";
import Button from "../components/Button";
import { ReactComponent as Logo } from "../images/svg/pageNotFound.svg";

export default function ErrorPageNotFound() {
  const history = useHistory();

  const handleGoBack = () => {
    history.goBack();
  };

  return (
    <Container style={{ height: "40%", width: "40%" }}>
      <div>
        <Logo />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button
            style={{ justifyContent: "center", width: "50%" }}
            onClick={handleGoBack}
          >
            Go Back
          </Button>
        </div>
      </div>
    </Container>
  );
}
