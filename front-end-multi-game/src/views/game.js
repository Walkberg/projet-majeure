import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Unity, { UnityContent } from "react-unity-webgl";
import { Grid } from "@material-ui/core";
import { useSelector } from "react-redux";
import QrCode from "../components/Qrcode";

const Game = () => {
  const [isGameExist, setIsGameExist] = useState(false);

  const { currentGame } = useSelector((state) => ({
    currentGame: state.game.currentGame,
  }));

  const unityContent = new UnityContent(
    `${currentGame.gameUrl}/Build/Build.json`,
    `${currentGame.gameUrl}/Build/UnityLoader.js`
  );

  useEffect(() => {
    console.log("do nothing");
    if (currentGame) {
      if (currentGame.gameUrl) setIsGameExist(true);
    } else {
      setIsGameExist(false);
    }
    console.log("Le jeux n'existe pas");
  }, [currentGame]);

  return (
    <Grid container>
      {isGameExist ? (
        <Unity unityContent={unityContent} />
      ) : (
        <div> Le jeux n'existe pas</div>
      )}
      <QrCode />
    </Grid>
  );
};

export default Game;
