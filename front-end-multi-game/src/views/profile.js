import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withRouter, useHistory } from "react-router-dom";
import { useIntl } from "react-intl";

import { resetPassword } from "../api/backend-node";

import Button from "../components/Button";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    padding: theme.spacing(3),
    display: "flex",
    flexDirection: "column"
  }
}));

export default function SignIn() {
  const classes = useStyles();
  const history = useHistory();
  const intl = useIntl();

  const handleResetPassword = async () => {
    console.log("send email");

    try {
      const response = await resetPassword();
      console.log("ok");
    } catch (error) {
      console.log("error on password");
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <Paper className={classes.paper}>
        <Button onClick={handleResetPassword}>Reset password</Button>
      </Paper>
    </Container>
  );
}
