import React, { useState } from "react";
import Container from "@material-ui/core/Container";

import { socket } from "../config/SocketIO";
import SocketIOLabels from "../constants/socketIoLabels";

import Button from "../components/Button";
import { TextField } from "@material-ui/core";
import { useDispatch } from "react-redux";

export default function Test() {
  const [roomId, setRoomId] = useState("");
  const [connected, setConnected] = useState(false);

  const dispatch = useDispatch();

  const handleJoinRoom = () => {
    setConnected(true);
    socket.emit(SocketIOLabels.JOIN_ROOM, roomId);
    console.log("join room");
  };

  const handleReady = () => {
    socket.emit(SocketIOLabels.TOOGLE_PLAYER_READY);
  };

  const handleChange = e => {
    console.log(e.target.value);
    setRoomId(e.target.value);
  };

  return (
    <Container component="main" maxWidth="xs">
      <TextField
        id="standard-helperText"
        label="Room id"
        defaultValue=""
        helperText="insert room id here"
        onChange={handleChange}
      />
      <Button onClick={handleJoinRoom}>Join room</Button>
      {connected && <Button onClick={handleReady}>Ready</Button>}
    </Container>
  );
}
