import React from "react";

import store from "./ducks/rootStore";
import { Provider } from "react-redux";

import CssBaseline from "@material-ui/core/CssBaseline";
import { IntlProviderWrapper } from "./config/IntlProviderWrapper";
import ThemeProvider from "./theme/ThemeProvider";
import Router from "./routes";
import { ACCESS_TOKEN } from "./constants";
import { connect } from "./utils/connexion";

import { BrowserRouter } from "react-router-dom";

import SocketIOContainer from "./container/socketIOContainer";

import { socket } from "./config/SocketIO";

import "./App.css";

if (localStorage[ACCESS_TOKEN]) {
  connect(localStorage[ACCESS_TOKEN]);
}

socket.on("connect", function (data) {});

function App() {
  return (
    <Provider store={store}>
      <IntlProviderWrapper>
        <ThemeProvider>
          <CssBaseline />
          <BrowserRouter>
            <SocketIOContainer>
              <Router />
            </SocketIOContainer>
          </BrowserRouter>
        </ThemeProvider>
      </IntlProviderWrapper>
    </Provider>
  );
}

export default App;
