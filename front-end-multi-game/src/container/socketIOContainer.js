import React, { useEffect } from "react";
import { socket } from "../config/SocketIO";
import store from "../ducks/rootStore";
import {
  setCurrentRoom,
  AddPlayerToRoom,
  setPlayerReady,
  removePlayerFromRoom,
} from "../ducks/matchMaking";

import SocketIOLabels from "../constants/socketIoLabels";
import { useHistory } from "react-router";
import { ScreenLabels } from "../constants";

function initSocketIO(history) {
  socket.emit(SocketIOLabels.ASK_ROOM, () => {
    console.log("ask room");
  });

  socket.emit("GameClientJoin", "fdshfiodsoi");

  socket.on(SocketIOLabels.GIVE_ROOM, (roomId) => {
    console.log(`Succesufuly join : ${roomId}`);
    store.dispatch(setCurrentRoom(roomId));
  });

  socket.on("leave room");

  socket.on(SocketIOLabels.LAUNCH_GAME, () => {
    const state = store.getState();
    const roomId = state.matchMaking.room;
    console.log(roomId);
    history.push(`${ScreenLabels.GAME}/?room=${roomId}`);
  });

  socket.on("PLAYER_LEAVE", (req) => {
    store.dispatch(removePlayerFromRoom(req.playerId));
  });

  socket.on(SocketIOLabels.JOIN_ROOM, (player) => {
    console.log("someone join the room");
    store.dispatch(AddPlayerToRoom(player));
  });

  socket.on(SocketIOLabels.TOOGLE_PLAYER_READY, (client) => {
    store.dispatch(setPlayerReady(client));
  });

  socket.on(SocketIOLabels.CONNEXION);
}

export default function SocketIOContainer({ children }) {
  const history = useHistory();

  useEffect(() => {
    initSocketIO(history);
  }, []);

  return <>{children}</>;
}
