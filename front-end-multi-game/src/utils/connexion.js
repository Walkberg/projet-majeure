import { ACCESS_TOKEN } from "../constants";
import setAuthorizationToken from "../config/AuthorizationHeader";
import jwt from "jsonwebtoken";

import store from "../ducks/rootStore";
import { setCurrentUser, deleteCurrentUser } from "../ducks/auth";

export const disconnect = () => {
  setAuthorizationToken();
  store.dispatch(deleteCurrentUser());
  localStorage.removeItem(ACCESS_TOKEN);
};

export const connect = (token) => {
  localStorage.setItem(ACCESS_TOKEN, token);
  const decodedToken = jwt.decode(token);
  const currentTime = new Date().getTime() / 1000;
  if (currentTime < decodedToken.exp) {
    store.dispatch(setCurrentUser(jwt.decode(token)));
    setAuthorizationToken(token);
    return true;
  }
  disconnect();
  return false;
};
