import axios from "axios";
import { gameList } from "../../helpers/gamelist";

export async function getAllGames() {
  console.log("passe ici");
  try {
    const response = axios.get("http://localhost:3001/api/game");
    return (await response).data;
  } catch (error) {
    return gameList;
  }
}

export async function getGameByName(title) {
  try {
    const response = axios.get(`http://localhost:3001/api/game/title/${title}`);
    return (await response).data;
  } catch (error) {
    return gameList;
  }
}
