import axios from "axios";

export const login = async (payload) => {
  try {
    const response = await axios.post(
      "http://localhost:3001/api/auth/login",
      payload
    );
    return response.data;
  } catch (error) {
    throw new Error("Unable to get a token.");
  }
};

export const resetPassword = async () => {
  try {
    const response = await axios.post(
      "http://localhost:3001/api/auth/forgotPassword",
      {}
    );
    return response.data;
  } catch (error) {
    throw new Error("Error from server");
  }
};
