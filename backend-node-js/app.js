const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const gameRoutes = require("./routes/game");
const userRoutes = require("./routes/user");
const mesureStorageRoutes = require("./routes/mesureStorage");

//const googlesheet = require("./controllers/googleSheet");

const app = express();

mongoose
  .connect(
    "mongodb+srv://walkberg:projetMajeur@projetmajeur-orsa5.mongodb.net/test?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

app.use(bodyParser.json());

app.use("/api/game", gameRoutes);
app.use("/api/auth", userRoutes);
app.use("/api/measureStorage", mesureStorageRoutes);

module.exports = app;
