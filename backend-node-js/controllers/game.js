const Game = require("../models/monGoose/game");

exports.createGame = (req, res, next) => {
  delete req.body._id;
  const game = new Game({
    ...req.body
  });
  game
    .save()
    .then(() => res.status(201).json({ message: "crrer" }))
    .catch(error => res.status(400).json({ error }));
};

exports.updateGame = (req, res, next) => {
  Game.updateOne({ _id: req.params.id }, { ...req.body, _id: req.param.id })
    .then(game => res.status(200).json({ message: "objet modifié" }))
    .catch(error => res.status(400).json({ error }));
};

exports.deleteGame = (req, res, next) => {
  Game.deleteOne({ _id: req.params.id })
    .then(game => res.status(200).json({ message: "object delete" }))
    .catch(error => res.status(400).json({ error }));
};

exports.getOneGame = (req, res, next) => {
  console.log("try get one");
  Game.findOne({ _id: req.params.id })
    .then(game => res.status(200).json(game))
    .catch(error => res.status(400).json({ error }));
};

exports.getOneGameByName = (req, res, next) => {
  console.log("try get one by title");
  Game.findOne({ title: req.params.id })
    .then(game => res.status(200).json(game))
    .catch(error => res.status(400).json({ error }));
};

exports.getAllGames = (req, res, next) => {
  Game.find()
    .then(games => res.status(200).json(games))
    .catch(error => res.status(400).json({ error }));
};
