const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/monGoose/user");
const nodemailer = require("nodemailer");
const crypto = require("crypto");

exports.signUp = (req, res, next) => {
  console.log("coucocu");
  bcrypt
    .hash(req.body.password, 10)
    .then(hash => {
      const user = new User({
        email: req.body.email,
        username: req.body.username,
        password: hash
      });
      user
        .save()
        .then(() => res.status(201).json({ message: "utilisateur créee" }))
        .catch(error => res.status(400).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
};

exports.login = (req, res, next) => {
  console.log("try to login");
  User.findOne({
    $or: [
      { email: req.body.usernameOrEmail },
      { username: req.body.usernameOrEmail }
    ]
  })
    .then(user => {
      if (!user) {
        return res.status(401).json({ messaeg: "user not find" });
      }
      bcrypt
        .compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            return res.status(401).json({ messaeg: "mot de passe incorecte" });
          }
          res.status(200).json({
            userId: user._id,
            token: jwt.sign(
              { userId: user._id },
              "RANDOM_TOKEN_SECRET", // token
              {
                expiresIn: "48h"
              }
            )
          });
        })
        .catch(error => res.status(500).json({ error }));
    })
    .catch(error =>
      res.status(500).json({
        error
      })
    );
};

exports.forgotPassword = (req, res, next) => {
  console.log("coucocu");

  console.log();
  User.findOne({
    email: "samuel.bergerot@gmail.com"
  })
    .then(user => {
      if (user === null) {
        console.log("user not in database");
        res.status(403).json({ messaeg: "user not in database" });
      } else {
        const token = crypto.randomBytes(20).toString("hex");
        User.update({
          resetPasswordToken: token,
          resetPasswordExpires: Date.now() + 360000
        });

        const transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "noreply.moveit.cpe@gmail.com",
            pass: "poA6h5x?"
          }
        });

        const mailOptions = {
          from: "noreply.moveit.cpe@gmail.com",
          to: `${user.email}`,
          subject: "Link to reset password",
          text: `Coucou tu peux reset ton mot de passe en cliqaunt sur le lien suivant:  http://localhost:3000/reset/${token}`
        };

        transporter.sendMail(mailOptions, (err, response) => {
          if (err) {
            console.log("error append");
          } else {
            console.log("here the response ", response);
            res.status(200).json({ messaeg: "C'est bon email envoyé" });
          }
        });
      }
    })
    .catch(error => {});
};
