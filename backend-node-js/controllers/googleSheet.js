///////////////////////////////////////////////////////

// https://developers.google.com/identity/protocols/googlescopes?authuser=1
const shortID = require("shortid");
const { google } = require("googleapis");
const keys = require("../keys.json");

const client = new google.auth.JWT(keys.client_email, null, keys.private_key, [
  "https://www.googleapis.com/auth/spreadsheets"
]);

module.exports.setData = function(data) {
  client.authorize(function(err, tokens) {
    if (err) {
      return;
    } else {
      postData(client, data);
    }
  });
};

async function postData(cl, data) {
  const gsapi = google.sheets({ version: "v4", auth: cl });
  const updateOptions = {
    spreadsheetId: "1DReAk77EuQlJ5ZNAV0MtqZbHPpwbpMV08gzujaYFaZw",
    range: "Feuille 1!A2",
    valueInputOption: "USER_ENTERED",
    resource: {
      values: appendarray(data)
    }
  };

  let response = await gsapi.spreadsheets.values.append(updateOptions);

  //console.log(response);
}

const appendarray = data => {
  const id = shortID.generate();
  let data1 = JSON.parse(data);

  let index = 0;
  let array = [];
  data1.forEach(element => {
    let line = [];
    line = [id, index, ...element];
    array.push(line);
    index++;
  });

  return array;
};
