const express = require("express");
const router = express.Router();
const mesureStorage = require("../controllers/mesureStorage");

router.post("/", mesureStorage.createMesureStorage);

module.exports = router;
