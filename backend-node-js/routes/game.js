const express = require("express");
const router = express.Router();
const gameController = require("../controllers/game");
const gameControlController = require("../controllers/controller");
const auth = require("../middleware/auth");

router.post("/", gameController.createGame);
router.put("/:id", auth, gameController.updateGame);
router.delete("/:id", auth, gameController.deleteGame);
router.get("/:id", gameController.getOneGame);
router.get("/title/:id", gameController.getOneGameByName);
router.get("/", gameController.getAllGames);
router.post("/:id/controller", gameControlController.createController);

module.exports = router;
