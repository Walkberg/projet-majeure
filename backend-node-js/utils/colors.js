let colors = [
  "#4f54f5",
  "#865865",
  "#783783",
  "#753753",
  "#1ff1ff",
  "#ff1ff1",
  "#f4ff4f",
  "#11f11f",
  "#1f11f1",
];

function getRandomColor() {
  return colors[Math.floor(Math.random() * colors.length)];
}

module.exports = { getRandomColor: getRandomColor };
