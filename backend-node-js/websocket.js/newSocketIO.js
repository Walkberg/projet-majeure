const crypto = require("crypto");

const shortID = require("shortid");
let mobileClient = require("./models/MobileClient");
let webClient = require("./models/WebClient");
let gameClient = require("./models/GameClient");
let rooms = require("./models/Room");

let MobileClient = mobileClient.MobileClient;
let WebClient = webClient.WebClient;
let GameClient = gameClient.GameClient;
let Room = rooms.Room;

let roomsList = [];

exports = module.exports = function(io) {
  io.on("connection", socket => {
    let client = generateClient(socket);
    let clientId = client.id;

    socket.on("ASK_ROOM", function() {
      io.to(socket.id).emit("GIVE_ROOM", client.currentRoom);
      console.log(roomsList);
    });

    socket.on("JOIN_ROOM", function(roomId) {
      const currentRoom = roomsList[roomId];
      if (client instanceof MobileClient) {
        if (currentRoom) {
          if (client.currentRoom === "") {
            socket.join(roomId, function() {
              client.currentRoom = roomId;
              roomsList[roomId].mobileClient[client.id] = client;
              io.to(currentRoom.webClient.currentRoom).emit(
                "JOIN_ROOM",
                client
              );
            });
          }
        } else {
          console.log("la room que vous essayer de joindred n'existe pas");
        }
      }
      //console.log(roomsList);
    });

    socket.on("TOOGLE_PLAYER_READY", function() {
      client.isReady = !client.isReady;

      io.to(roomsList[client.currentRoom].webClient.socketId).emit(
        "TOOGLE_PLAYER_READY",
        client
      );

      const a = Object.keys(roomsList[client.currentRoom].mobileClient).filter(
        test =>
          roomsList[client.currentRoom].mobileClient[test].isReady === false
      );

      if (a.length === 0) {
        io.to(roomsList[client.currentRoom].webClient.socketId).emit(
          "LAUNCH_GAME"
        );
      }
    });

    socket.on("disconnect", function() {
      console.log("user Disconected");
      if (client instanceof WebClient) {
        delete roomsList[client.currentRoom];
        delete client;
      }
      if (client instanceof MobileClient) {
        delete client;
      }
    });
  });
};

generateClient = function(socket) {
  const type = socket.handshake.query["type"];

  switch (type) {
    case "gameClient":
      let gameClient = new GameClient(socket.id);
      return gameClient;
    case "webClient":
      let webClient = new WebClient(socket.id);
      let room = generateNewRoom(roomsList, webClient);
      socket.join(room.id, function() {
        webClient.currentRoom = room.id;
      });
      return webClient;
    case "mobileClient":
      let mobileClient = new MobileClient(socket.id);
      return mobileClient;
    default:
      return 0;
  }
};

generateNewRoom = function(roomsList, client) {
  let room = new Room();
  room.webClient = client;
  roomsList[room.id] = room;
  return room;
};
