const shortID = require("shortid");

function WebClient(socketId) {
  this.id = shortID.generate();
  this.type = "webClient";
  this.currentRoom = "";
  this.socketId = socketId;
  this.userId = "";
  this.isGuest = true;
  this.username = "Guest 5";
}

module.exports = { WebClient: WebClient };
