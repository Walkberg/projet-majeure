const shortID = require("shortid");
const remoteType = require("../../constants/remoteType");

function Room() {
  this.id = shortID.generate();
  this.status = "matchmaking";
  this.remoteType = remoteType.Joystick;
  this.roomName = "";
  this.webClient = {};
  this.gameClient = {};
  this.mobileClient = [];
}

module.exports = { Room: Room };
