let Button = require("./Button");
const KeyCode = require("../../constants/keyCode");

function initButton() {
  let buttons = [];
  buttons[KeyCode.Right] = new Button(KeyCode.Right);
  buttons[KeyCode.Left] = new Button(KeyCode.Left);
  return buttons;
}

function InputNetwork(playerId) {
  // Accept name and age in the constructor
  this.playerId = playerId;
  this.buttons = initButton();

  //this.buttonLeft = new Button(1);
  //this.buttonRight = new Button(2);*/
}

InputNetwork.prototype.getButton = function(id) {
  return this.buttons[id];
};

InputNetwork.prototype.setButton = function(id, isPress) {
  //console.log(buttonPress);
  if (this.buttons[id]) {
    this.buttons[id].isPress = isPress;
  }
};

module.exports = InputNetwork;
