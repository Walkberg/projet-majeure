const shortID = require("shortid");

function GameClient(socketId) {
  this.id = shortID.generate();
  this.currentRoom = "";
  this.type = "gameClient";
  this.socketId = socketId;
  this.userId = "";
  this.isGuest = true;
  this.username = "Guest 5";
}

module.exports = { GameClient: GameClient };
