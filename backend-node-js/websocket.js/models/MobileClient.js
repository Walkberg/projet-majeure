const colors = require("../../utils/colors");
const shortID = require("shortid");

function MobileClient(socketId) {
  this.id = shortID.generate();
  this.type = "mobileClient";
  this.currentRoom = "";
  this.socketId = socketId;
  this.isGuest = true;
  this.isReady = false;
  this.username = "Guest 5";
  this.color = colors.getRandomColor();
}

module.exports = { MobileClient: MobileClient };
