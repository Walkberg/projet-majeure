function Button() {
  this.isPress = false;
}

Button.prototype.getButtonPress = function() {
  return this.buttonPress;
};

Button.prototype.setButtonPress = function(isPress) {
  this.buttonPress = isPress;
};

module.exports = Button;
