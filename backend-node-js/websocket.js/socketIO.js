const googleSheetController = require("../controllers/googleSheet");

let mobileClient = require("./models/MobileClient");
let webClient = require("./models/WebClient");
let gameClient = require("./models/GameClient");
let rooms = require("./models/Room");

const RemoteType = require("../constants/remoteType");

let InputNetwork = require("./models/InputNetwork");

let MobileClient = mobileClient.MobileClient;
let WebClient = webClient.WebClient;
let GameClient = gameClient.GameClient;
let Room = rooms.Room;

let roomsList = [];

// code à reorganiser

exports = module.exports = function (io) {
  io.on("connection", (socket) => {
    console.log("user join");

    // init
    //let currentRoom;
    let client = generateClient(socket);
    let input = new InputNetwork(client.id);

    // matchmaking
    socket.on("ASK_ROOM", function () {
      io.to(socket.id).emit("GIVE_ROOM", client.currentRoom);
      console.log(roomsList);
    });

    socket.on("spawn", function () {
      console.log("spanw");
      io.emit("spawn", { id: client.id });
    });

    socket.on("PRESS_BUTTON", function (button) {
      console.log(button);
      button.value = JSON.parse(button.value);
      input.setButton(button.id, button.value);
      let currentRoom = roomsList[client.currentRoom];
      if (!currentRoom.gameClient) {
        return;
      }
      let gameSocket = currentRoom.gameClient.socketId;
      const newButton = {
        playerId: client.id,
        button: button,
      };
      console.log(newButton);
      io.to(gameSocket).emit("UPDATE_BUTTON", newButton);
    });

    socket.on("MOVE_DETECTED", function (direction) {
      console.log("move deected");
      console.log(direction);
      let currentRoom = roomsList[client.currentRoom];
      if (!currentRoom.gameClient) {
        return;
      }
      let gameSocket = currentRoom.gameClient.socketId;
      const dir = { playerId: client.id, direction: direction.direction };

      // to change

      io.to(gameSocket).emit("MOVE_DETECTED", dir);

      const measures = direction.measures;
      const postData = { playerId: "coucou", measures: measures };
      googleSheetController.setData(measures);

      io.to("pythonRoom").emit("POST_ACCELEROMETER_DATA", postData);
    });

    socket.on("RESPONSE_MOUVEMENT", function (data) {
      console.log("Response mouvement");
      const playerId = data.playerId;
      const direction = data.direction;

      // emit to game
      /*let currentRoom = roomsList[playerId.currentRoom];
      if (currentRoom.gameClient) {
        let gameSocket = currentRoom.gameClient.socketId;
      }*/
      //const dir = { playerId: playerId, direction: direction };
      console.log(data);
      // to change
      // io.to(gameSocket).emit("MOVE_DETECTED", dir);
    });

    socket.on("JOYSTICK_MOVE", function (input) {
      console.log("joystick move");
      let currentRoom = roomsList[client.currentRoom];
      if (!currentRoom.gameClient) {
        return;
      }
      let gameSocket = currentRoom.gameClient.socketId;
      const dir = { playerId: client.id, axis: input };
      io.to(gameSocket).emit("UPDATE_AXIS", dir);
      console.log(input);
    });

    socket.on("POST_ACCELEROMETER_DATA", function (data) {
      console.log("post accelometer");
      googleSheetController.setData(data);
      io.to("pythonRoom").emit("POST_ACCELEROMETER_DATA", data);
    });

    socket.on("CHANGE_REMOTE", function (remoteType) {
      // get current client rrom
      let currentRoom = client.currentRoom;
      // on envoie à toute la room qu'on essaye de changer de mannette
      if (roomsList[currentRoom]) {
        roomsList[currentRoom].remoteType = remoteType;
        io.to(currentRoom).emit("CHANGE_REMOTE", { remote: remoteType });
      }
    });

    socket.on("JOIN_ROOM", function (req) {
      console.log("Join room");
      console.log(req);
      let roomId = req.roomId;
      let currentRoom = roomsList[roomId];
      console.log("user try to join room");
      console.log(client);
      if (client instanceof MobileClient) {
        roomId = req.roomId;
        console.log(roomId);
        if (currentRoom) {
          if (client.currentRoom === "") {
            socket.join(roomId, function () {
              client.currentRoom = roomId;
              roomsList[roomId].mobileClient[client.id] = client;
              socket.emit("JOIN_ROOM", { remote: currentRoom.remoteType });
              io.to(currentRoom.webClient.socketId).emit("JOIN_ROOM", client);
              if ((roomsList[roomId].status = "game")) {
                io.to(currentRoom.gameClient.socketId).emit("spawn", {
                  id: client.id,
                  color: client.color,
                });
              }
            });
          }
        } else {
          socket.emit("ROOM_UNDEFINED");
          console.log("la room que vous essayer de joindred n'existe pas");
        }
      } else if (client instanceof GameClient) {
        console.log("type gameClient");
        roomId = req;
        console.log(roomId);
        currentRoom = roomsList[roomId];
        if (currentRoom) {
          if (client.currentRoom === "") {
            socket.join(roomId, function () {
              client.currentRoom = roomId;
              roomsList[roomId].gameClient = client;
              roomsList[roomId].status = "game";
              io.to(currentRoom.webClient.currentRoom).emit(
                "GAME_JOIN",
                client
              );

              const mobileClients = roomsList[roomId].mobileClient;
              for (const index in mobileClients) {
                console.log("spawn user");

                console.log(socket.id);
                socket.emit("spawn", {
                  id: mobileClients[index].id,
                  color: mobileClients[index].color,
                });
              }
            });
          }
        } else {
          console.log("la room que vous essayer de joindred n'existe pas");
        }
      }
      console.log(roomsList);
    });

    socket.on("TOGGLE_PLAYER_READY", function () {
      client.isReady = !client.isReady;
      io.to(roomsList[client.currentRoom].webClient.socketId).emit(
        "TOOGLE_PLAYER_READY",
        client
      );

      const playerNoReady = Object.keys(
        roomsList[client.currentRoom].mobileClient
      ).filter(
        (test) =>
          roomsList[client.currentRoom].mobileClient[test].isReady === false
      );

      if (playerNoReady.length === 0) {
        io.to(roomsList[client.currentRoom].webClient.socketId).emit(
          "LAUNCH_GAME"
        );
      }
    });

    // post data
    socket.on("JOIN_PYTHON_ROOM", function () {
      socket.join("pythonRoom", function () {
        socket.emit("JOIN_PYTHON_ROOM");
      });
    });

    // disconect
    socket.on("disconnect", function () {
      const currentClient = client;
      delete client;
      console.log("user Disconected");
      if (roomsList[currentClient.currentRoom]) {
        if (client instanceof WebClient) {
          delete roomsList[client.currentRoom];
        }
        if (client instanceof MobileClient) {
          if (roomsList[currentClient.currentRoom].mobileClient)
            delete roomsList[currentClient.currentRoom].mobileClient[
              currentClient.id
            ];

          socket
            .to(currentClient.currentRoom)
            .emit("PLAYER_LEAVE", { playerId: currentClient.id });
          console.log(roomsList);
        }
        if (client instanceof GameClient) {
          const clientroom = client.currentRoom;
          delete roomsList[currentClient.currentRoom].gameClient;
          console.log(roomsList[clientroom]);
        }
      }
    });
  });
};

generateClient = function (socket) {
  const type = socket.handshake.query["type"];

  console.log("on veut crre nouveau utilisateru ");
  console.log(type);

  switch (type) {
    case "gameClient?EIO=4":
      console.log("J creer un game client");
      let gameClient = new GameClient(socket.id);
      return gameClient;
    case "webClient":
      let webClient = new WebClient(socket.id);
      let room = generateNewRoom(roomsList, webClient);
      socket.join(room.id, function () {
        webClient.currentRoom = room.id;
      });
      return webClient;
    case "mobileClient":
      let mobileClient = new MobileClient(socket.id);
      return mobileClient;
    default:
      console.log("type de joueur introvable");
      return 0;
  }
};

generateNewRoom = function (roomsList, client) {
  let room = new Room();
  room.webClient = client;
  roomsList[room.id] = room;
  return room;
};
