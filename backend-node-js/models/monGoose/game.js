const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const gameSchema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  rule: { type: String, required: true },
  imageUrl: { type: String, required: true },
  gameUrl: { type: String, required: true },
  test: { type: String, required: false },
  controls: [
    {
      type: Schema.Types.ObjectId,
      ref: "GameControl",
    },
  ],
});

module.exports = mongoose.model("Game", gameSchema);
