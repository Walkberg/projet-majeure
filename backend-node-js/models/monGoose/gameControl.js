const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const gameSchema = new Schema({
  button: { type: String, required: true },
  color: { type: String, required: true },
  game: {
    type: Schema.Types.ObjectId,
    ref: "Game"
  }
});

module.exports = mongoose.model("GameControl", gameSchema);
