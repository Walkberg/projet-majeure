const RemoteType = {
  Joystick: "joystick",
  Accelerometer: "accelerometer"
};

module.exports = RemoteType;
