const KeyCode = {
  Right: "right",
  Left: "left",
  Up: "Up",
  Down: "Down"
};

module.exports = KeyCode;
