from __future__ import print_function
from mlxtend.classifier import MultiLayerPerceptron as MLP
from sklearn.neural_network import MLPClassifier
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import gspread
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from mpl_toolkits.mplot3d import Axes3D
import gspread
import json
import time

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('move-it.json', scope)
client = gspread.authorize(creds)

#import socketio
import socketio

sio = socketio.Client()
sio.connect('http://192.168.43.95:3001/?type=pythonClient')
sio.emit('JOIN_PYTHON_ROOM')


def copy(mo):
    # mo : movement code

    #Enter the Spreadsheet ID you want to copy data
    RAW_SPREADSHEET_ID = '1DReAk77EuQlJ5ZNAV0MtqZbHPpwbpMV08gzujaYFaZw'
    SAMPLE_RANGE_NAME = 'Feuille 1!A2:E'

    sheet1 = client.open('Gyroscope&Accelerometre').sheet1
    values = sheet1.get_all_values()
    lastest_data = []
    #Get the 100 latest movements
    if int(values[-1][1]) == 99:
        lastest_data = values[-100:]
    #Delete the ID of the phone and add the movement number
        for i in range(0,len(lastest_data)):
            lastest_data[i].pop(0)
            lastest_data[i].append(mo)

    return lastest_data

def paste(data):
    # data : data to paste

    #Enter the Spreadsheet ID you want to paste data
    RAW_SPREADSHEET_ID = '1Uq4B6ObzowVV0lmPxr0IKP6IfuS-D5i6QZIYFM-d7rI'

    #Get the second sheet
    sheet2 = client.open("DonneesEtiquetees").get_worksheet(0)
    #Initialize the variables
    i = 0
    k = 0

    #changing the range to paste our data in the first empty row
    firstemptyrow=1
    clist = sheet2.range('A1:E10000')
    for cell in clist:
        if cell.value=='':
            firstemptyrow=cell.row
            break

    strclist = 'A'+str(firstemptyrow)+':E'+str(firstemptyrow+99)
    cell_list = sheet2.range(strclist)
    for cell in cell_list:
        #If lastest column: go to next row
        if i==5:
            i=0
            k=k+1
        cell.value = data[k][i]
        i=i+1

    #Paste the data to range specified
    sheet2.update_cells(cell_list)

def translate(mouv_nb):
    #Enter the Spreadsheet ID you want to copy data
    RAW_SPREADSHEET_ID = '1Jzp8dw29-RR11-1vmqyUv_jre51yB728L2WGXU46tFc'
    SAMPLE_RANGE_NAME = 'Feuille 1!A1:B'

    sheet1 = client.open('CorrespondanceMouvements').sheet1
    movements = sheet1.get_all_values()

    mouv_str = "RIEN"
    #Delete the ID of the phone and add the movement number
    for i in range(0,len(movements)):
        if float(movements[i][0]) == mouv_nb:
            mouv_str = movements[i][1]
        i=i+1
    return mouv_str

def init() :
    creds = ServiceAccountCredentials.from_json_keyfile_name('move-it.json', scope)
    client = gspread.authorize(creds)
    SAMPLE_RANGE_NAME = 'Feuille 1!A2:E'

    SAMPLE_SPREADSHEET_ID = '1Uq4B6ObzowVV0lmPxr0IKP6IfuS-D5i6QZIYFM-d7rI'
    sheet = client.open('DonneesEtiquetees').sheet1

    data = sheet.get_all_values()

    return data

def shaping(data, k, col):
    # data : table of data
    # K : first point of the movement
    # col : first column of the movement

    # The movement is composed of 100 lines
    movement = data[k: k+100]

    # If the last is not the 99th, the movement is not full and/or of 100 points
    # so we do not treat this movement
    if(movement ==  []) or(int(movement[-1][col]) != 99):
        return(0,0,0)

    x=[]
    y=[]
    z=[]

    for i in range(0, len(movement)):
        x.append(float(movement[i][col + 1]))
        y.append(float(movement[i][col + 2]))
        z.append(float(movement[i][col + 3]))
    return(x,y,z)

def train():
    data = init()
    labels_train = []
    labels_test = []
    train_length = len(data) * 0.7

    index = range(1,1) # Initial length of the dataframe
    columns = [] # Labels of the dataframe columns
    for i in range(0,100):
        columns.append('x' + str(i))
    for i in range(0,100):
        columns.append('y' + str(i))
    for i in range(0,100):
        columns.append('z' + str(i))

    k = 1 # Dummy variables for the while loop
    ligne = 0

    # Initialisation of the dataframe
    dataframe_train = pd.DataFrame(index=index, columns=columns)
    dataframe_test = pd.DataFrame(index=index, columns=columns)

    # Constructing the dataframe to fit the model
    while k < train_length - 100 :
        x, y, z = shaping(data, k, 0)
        # If the movement is usable
        if (x,y,z) != (0,0,0) :
            for i in range(0,100):
                dataframe_train.at[ligne,'x' + str(i)] = x[i]
                dataframe_train.at[ligne,'y' + str(i)] = y[i]
                dataframe_train.at[ligne,'z' + str(i)] = z[i]
            ligne += 1
            labels_train.append(float(data[k + 99][4]))
            k += 100

        elif data[k : k+100] != [] :
            k += 1
            #Looking for the first point of a movement
            while int(data[k][0]) !=0:
                k += 1

        else:
            break

    ligne = 0
    # Constructing the dataframe to test the model
    while k < len(data) - 100 :
        x, y, z = shaping(data, k, 0)
        # If the movement is usable
        if (x,y,z) != (0,0,0) :
            for i in range(0,100):
                dataframe_test.at[ligne,'x' + str(i)] = x[i]
                dataframe_test.at[ligne,'y' + str(i)] = y[i]
                dataframe_test.at[ligne,'z' + str(i)] = z[i]
            ligne += 1
            labels_test.append(float(data[k + 99][4]))
            k += 100

        elif data[k : k+100] != [] :
            k += 1
            #Looking for the first point of a movement
            while int(data[k][0]) !=0:
                k += 1

        else:
            break


    # Declaring Model
    mlp = MLPClassifier(hidden_layer_sizes=(14,7,7, ), activation='relu', solver='lbfgs', max_iter=200)

    # Fitting
    mlp.fit(dataframe_train, labels_train)
    score = mlp.score(dataframe_test, labels_test)
    print("Number of labeled movements : " + str(len(dataframe_train)) + " (train) + " + str(len(dataframe_test)) + " (test)")
    print("Accuracy : ", score)

    return mlp

def predict(model, data):
    ret = [0] # We assume the user did nothing if we do not understand

    # If we do understand
    if  np.shape(data) == (1,300) :
        ret = model.predict(data)

    return ret


@sio.on('POST_ACCELEROMETER_DATA')
def data_received(data):
    print('Data received')
    print('Predicting...')
    start = time.time()

    index = range(1,1) # Initial length of the dataframe
    columns = [] # Labels of the dataframe columns
    for i in range(0,100):
        columns.append('x' + str(i))
    for i in range(0,100):
        columns.append('y' + str(i))
    for i in range(0,100):
        columns.append('z' + str(i))

    # Initialisation of the dataframe
    dataframe = pd.DataFrame(index=index, columns=columns)
    print(data)
    print(json.loads(data['measures']))
    tempo = json.dumps(data['playerId'])
    playerId = tempo.strip('"')
    print(str(tempo))
    for i in range(0, len(json.loads(data['measures']))):
        dataframe.at[0,'x' + str(i)] = float(json.loads(data['measures'])[i][0])
        dataframe.at[0,'y' + str(i)] = float(json.loads(data['measures'])[i][1])
        dataframe.at[0,'z' + str(i)] = float(json.loads(data['measures'])[i][2])

    answer = translate(predict(trained_model, dataframe)[0])
    x = {
      "playerId":playerId ,
      "direction": answer
      }
    jsontosend=json.dumps(x)
    print(jsontosend)
    sio.emit('RESPONSE_MOUVEMENT', jsontosend)
    print(answer)
    print (time.time() - start, "seconds to predict")
    print(json.loads(data['playerId'])
    code_movement = int(input("Enter the code of the movment : "))
    paste(copy(code_movement))
    print('copy/paste done')


# if __name__ == '__main__':
print('Training...')
start = time.time()
global trained_model
trained_model = train()
print('Trained !')
print (time.time() - start, "seconds to train")
