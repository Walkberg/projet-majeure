﻿using Project.Networking;
using Project.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.InputNetwork
{
    public class InputManager : MonoBehaviour
    {
        [Header("Class References")]
        [SerializeField]
        private NetworkIdentity networkIdentity;

        private Axis axis;
        private Accelerometer accelerometer;
        public Dictionary<string, Button> buttons = new Dictionary<string, Button>();

        private string movement;


        public void Start()
        {
            axis = new Axis();
            accelerometer = new Accelerometer();
            InitButtons();
        }

        private void InitButtons()
        {
            buttons.Add(KeyCodeConst.Right, new Button());
            buttons.Add(KeyCodeConst.Left, new Button());
            buttons.Add(KeyCodeConst.Up, new Button());
            buttons.Add(KeyCodeConst.Down, new Button());
        }

        public void Update()
        {
            if (networkIdentity.IsControlling())
            {

            }
        }


        // at the end of each update reset all button is press value
        // reset accelerometer value
        void LateUpdate()
        {
            foreach (KeyValuePair<string, Button> button in buttons)
            {
                button.Value.ResetIsPressed();
            }
            accelerometer.SetAccelerometer(AccelerometerConst.None);

        }

        public float GetAxis(string id)
        {
            return axis.GetAxis(id);
        }

        public void SetAxis(float x, float y)
        {
            axis.SetAxis(x, y);
        }

        public bool GetButtonDown(string id)
        {
            return buttons[id].GetValue();
        }

        public void SetButtonDown(string id, bool value)
        {
            buttons[id].SetValue(value);
        }

        public bool GetMovement(string mov)
        {
            return movement == mov ? true : false;
        }

        public string GetMovement()
        {
            return movement;
        }
        public void SetMovement(string direction)
        {
            movement = direction;
        }
    }
}