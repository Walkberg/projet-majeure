using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.InputNetwork
{
    public class Accelerometer
    {
        private string currentMovement;
        private string lastMovement;

        public string GetAccelerometer()
        {
            return currentMovement;
        }

        public string GetAccelerometerLast()
        {
            return lastMovement;
        }

        public bool GetAccelerometer(string id)
        {
            return currentMovement == id ? true : false;
        }

        public void SetAccelerometer(string movement)
        {
            lastMovement = currentMovement;
            currentMovement = movement;
        }
    }
}