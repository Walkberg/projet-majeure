﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.InputNetwork
{
    public class Button
    {
        private bool isPressed = false;
        private bool isDown = false;

        public bool GetValue()
        {
            return isPressed;
        }

        public void SetValue(bool value)
        {
            isPressed = value;
        }

        public void ResetIsPressed()
        {
            isPressed = false;
        }
    }
}