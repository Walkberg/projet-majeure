﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.InputNetwork
{
    public class KeyCodeConst
    {
        public static string Right = "right";
        public static string Left = "left";
        public static string Up = "up";
        public static string Down = "down";
        public static string A = "button_a";
        public static string B = "button_b";
        public static string X = "button_x";
        public static string Y = "button_y";
    }

}