using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.InputNetwork
{
    public class Axis
    {
        private float horizontalAxis;
        private float verticalAxis;

        // on pourait faire une transition douce des inputs

        public float GetAxis(string axisId)
        {
            if (axisId == AxisConst.Horizontal)
                return horizontalAxis;
            else if (axisId == AxisConst.Vertical)
                return verticalAxis;
            return 0;
        }

        public void SetAxis(float x, float y)
        {

            horizontalAxis = x;
            verticalAxis = y;
        }
    }
}