﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.InputNetwork
{
    public class AxisConst
    {
        public static string Horizontal = "Horizontal";
        public static string Vertical = "Vertical";
    }

}