using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.InputNetwork;


namespace Project.Player
{
    public class BetterJumping : MonoBehaviour
    {
        private Rigidbody2D rb;
        private InputManager inputManager;
        public float fallMultiplier = 2.5f;

        public float lowJumpMultiplier = 2f;

        void Start()
        {
            inputManager = GetComponent<InputManager>();
            rb = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y > 0 && !inputManager.GetButtonDown(KeyCodeConst.Up))
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            }
        }
    }
}