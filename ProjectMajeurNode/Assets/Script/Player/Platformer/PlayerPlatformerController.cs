﻿using System.Collections;
using System.Collections.Generic;
using Project.InputNetwork;
using UnityEngine;
using UnityEngine.UI;


namespace Project.Player
{
    public class PlayerPlatformerController : MonoBehaviour
    {

        public Rigidbody2D rb;
        public InputManager inputManager;
        private PlayerColision coll;
        private PlayerPlatformerAnimation anim;

        [Space]
        [Header("Stats")]
        private float speed = 5f;
        private float jumpForce = 12f;
        private float slideSpeed = 10f;
        private float dashSpeed = 20f;


        [Space]
        [Header("Booleans")]
        public bool canMove;
        public bool wallGrab;
        public bool isDashing;
        private bool wallSlide;

        [Space]
        private bool onGround;
        private bool hasDashed;

        private int nbJump = 0;
        private int nbExtraJump = 1;

        private float dampenWebInput = 0.25f;


        private string previousMovement;

        private int side = 1;

        // Start is called before the first frame update
        void Start()
        {
            coll = GetComponent<PlayerColision>();
            rb = GetComponent<Rigidbody2D>();
            inputManager = GetComponent<InputManager>();
            anim = GetComponentInChildren<PlayerPlatformerAnimation>();
        }

        // Update is called once per frame


        void FixedUpdate()
        {
            var horizontalInput = inputManager.GetAxis("Horizontal");
            rb.velocity = new Vector2(horizontalInput * speed, rb.velocity.y);
        }


        void Update()
        {

            var horizontalInput = inputManager.GetAxis("Horizontal");
            var verticalInput = inputManager.GetAxis("Vertical");

            previousMovement = inputManager.GetMovement();
            if (Input.GetButton("Fire3"))
                Debug.Log("vjkdbvjxbivk");


            if (coll.onWall && Input.GetButton("Fire3"))
            {
                if (side != coll.wallSide)
                    anim.Flip(side * -1);
                wallGrab = true;
                wallSlide = false;
            }

            if (wallGrab && !isDashing)
            {
                rb.gravityScale = 0;
                if (horizontalInput > .2f || horizontalInput < -.2f)
                    rb.velocity = new Vector2(rb.velocity.x, 0);

                float speedModifier = verticalInput > 0 ? .5f : 1;

                rb.velocity = new Vector2(rb.velocity.x, verticalInput * (speed * speedModifier));
            }

            if (horizontalInput > 0)
            {
                side = 1;
                anim.Flip(side);
            }
            if (horizontalInput < 0)
            {
                side = -1;
                anim.Flip(side);
            }

            //if (coll.onGround() && inputManager.GetButtonDown(KeyCodeConst.Up))
            if (inputManager.GetButtonDown(KeyCodeConst.Up))
            {
                if (coll.onGround || nbJump < nbExtraJump)
                {
                    if (coll.onGround)
                        nbJump = 0;
                    else
                        nbJump++;
                    Jump(Vector2.up);
                }
            }
            anim.SetHorizontalMovement(horizontalInput, rb.velocity.y);
        }

        private void Jump(Vector2 dir)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.velocity += dir * jumpForce;
        }
    }
}
