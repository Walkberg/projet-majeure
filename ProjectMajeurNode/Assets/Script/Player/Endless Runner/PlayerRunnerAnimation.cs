﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Player
{
    public class PlayerRunnerAnimation : MonoBehaviour
    {

        private Animator anim;
        private PlayerControllerRunner move;
        private PlayerColision coll;
        [HideInInspector]
        public SpriteRenderer sr;

        void Start()
        {
            anim = GetComponent<Animator>();
            coll = GetComponentInParent<PlayerColision>();
            move = GetComponentInParent<PlayerControllerRunner>();
            sr = GetComponent<SpriteRenderer>();
        }

        void Update()
        {
            anim.SetBool("onGround", coll.onGround);
            anim.SetBool("onWall", coll.onWall);
            anim.SetBool("onRightWall", coll.onRightWall);
            anim.SetBool("canMove", move.canMove);
            anim.SetBool("isSliding", move.isSliding);
            anim.SetBool("isStarted", move.isStarted);
        }

        public void SetHorizontalMovement(float x, float yVel)
        {
            anim.SetFloat("horizontalAxis", Mathf.Abs(x));
            anim.SetFloat("verticalVelocity", yVel);
        }

        public void SetTrigger(string trigger)
        {
            anim.SetTrigger(trigger);
        }

        public void Flip(int side)
        {
            bool state = (side == 1) ? false : true;
            sr.flipX = state;
        }
    }
}
