﻿using System.Collections;
using System.Collections.Generic;
using Project.InputNetwork;
using UnityEngine;


namespace Project.Player
{
    public class PlayerControllerRunner : MonoBehaviour
    {
        public Rigidbody2D rb;
        public InputManager inputManager;
        private PlayerColision playerColision;
        private PlayerAnimation anim;

        [Space]
        [Header("Stats")]
        private float speed = 3f;
        private float jumpForce = 15f;


        private float slideDuration = 1.5f;
        private float slideTimer;


        private int nbJump = 0;
        private int nbExtraJump = 1;

        [Space]
        [Header("Booleans")]

        public bool isStarted = false;
        public bool canMove;
        public bool canSlide;
        public bool canJump;
        public bool isSliding;

        [Space]
        private bool onGround;
        private bool hasDashed;

        public bool isAlive = true;


        [SerializeField] private ParticleSystem deathParticle;


        private string previousMovement;

        private int side = 1;

        // Start is called before the first frame update
        void Start()
        {
            playerColision = GetComponent<PlayerColision>();
            rb = GetComponent<Rigidbody2D>();
            inputManager = GetComponent<InputManager>();
            anim = GetComponentInChildren<PlayerAnimation>();

            deathParticle.Stop();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log(inputManager.GetMovement());
            if (Input.GetKeyDown(KeyCode.P))
            {
                isStarted = true;
            }
            if (isStarted && isAlive)
            {
                Mouvement();

                if (isSliding)
                {
                    Slide();
                }
                previousMovement = inputManager.GetMovement();

                Debug.Log("coucpou toi");

                if (canSlide && (previousMovement == "BELOW" || inputManager.GetButtonDown(KeyCodeConst.Down) || Input.GetKeyDown(KeyCode.A)))
                {
                    isSliding = true;
                    slideTimer = 0;
                    playerColision.SetCollider(false);

                }

                if (playerColision.onGround)
                {
                    canSlide = true;
                }

                //if (coll.onGround() && inputManager.GetButtonDown(KeyCodeConst.Up))
                if (previousMovement == "ABOVE" || inputManager.GetButtonDown(KeyCodeConst.Up) || Input.GetKeyDown("space"))
                {
                    if ((playerColision.onGround || nbJump < nbExtraJump) && !playerColision.onWallUp)
                    {
                        if (playerColision.onGround)
                            nbJump = 0;
                        else
                            nbJump++;
                        canSlide = false;
                        isSliding = false;
                        playerColision.SetCollider(true);
                        Jump(Vector2.up);
                    }
                }
            }
        }

        private void Jump(Vector2 dir)
        {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.velocity += dir * jumpForce;
        }


        private void Mouvement()
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }

        private void Slide()
        {
            if (slideTimer > slideDuration && !playerColision.onWallUp)
            {
                playerColision.SetCollider(true);
                isSliding = false;
            }
            else
                slideTimer += Time.deltaTime;
        }


        public Vector3 GetPosition()
        {
            return transform.position;
        }


        public void ReceiveDamage()
        {
            StartCoroutine(Death());
        }

        IEnumerator Death()
        {
            isAlive = false;
            deathParticle.Play();
            yield return new WaitForSeconds(.9f);
            deathParticle.Stop();
        }
    }
}
