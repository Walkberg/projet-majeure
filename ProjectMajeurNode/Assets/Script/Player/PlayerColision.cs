﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Player
{
    public class PlayerColision : MonoBehaviour
    {
        // Start is called before the first frame update
        [Header("Layers")]
        public LayerMask groundLayer;
        public LayerMask upLayer;

        [SerializeField]
        private Collider2D collider;

        [SerializeField]
        private PlayerControllerRunner controller;

        [Space]


        public bool onGround;
        public bool onWall;
        public bool onRightWall;
        public bool onLeftWall;
        public int wallSide;
        public bool onWallUp;


        public float playerSize = 0.4f;

        [Space]

        [Header("Collision")]

        public float collisionRadius = 0.25f;
        public Vector2 bottomOffset, rightOffset, leftOffset;
        private Color debugCollisionColor = Color.red;

        private RaycastHit hit;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
            onWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, collisionRadius, groundLayer)
                || Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, collisionRadius, groundLayer);


            onRightWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, collisionRadius, groundLayer);
            onLeftWall = Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, collisionRadius, groundLayer);

            onWallUp = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.up), playerSize, upLayer, -Mathf.Infinity, Mathf.Infinity);

            wallSide = onRightWall ? -1 : 1;
        }

        public void SetCollider(bool activate)
        {
            collider.enabled = activate;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            var positions = new Vector2[] { bottomOffset, rightOffset, leftOffset };
            Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, collisionRadius);
            Gizmos.DrawWireSphere((Vector2)transform.position + rightOffset, collisionRadius);
            Gizmos.DrawWireSphere((Vector2)transform.position + leftOffset, collisionRadius);
            Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * playerSize);

        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag.Equals("Damage"))
            {

                Debug.Log("Trigger Damage");
                controller.ReceiveDamage();
            }
            else if (col.gameObject.tag.Equals("Collectable"))
            {
                Destroy(col.gameObject);
                Debug.Log("colect");
            }

        }
    }
}