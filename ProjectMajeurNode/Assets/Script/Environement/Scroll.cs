﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour
{


    private float scrollSpeed = 0.1f;
    private Material material;
    // Start is called before the first frame update
    void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 offset = new Vector2(scrollSpeed, 0);
        material.mainTextureOffset = offset * Time.time;

    }
}
