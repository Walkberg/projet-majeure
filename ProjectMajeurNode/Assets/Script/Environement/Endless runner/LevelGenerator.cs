﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Player;

public class LevelGenerator : MonoBehaviour
{


    private const float PLAYER_DISTANCE_SPAWN_LEVEL_PART = 20f;
    [SerializeField] public MutlipleTargetCamera camera;
    [SerializeField] private Transform levelPartStart;
    [SerializeField] private List<Transform> levelPartList;
    [SerializeField] private PlayerControllerRunner player;


    private Vector3 lastEndPosition;
    // Start is called before the first frame update
    void Awake()
    {
        lastEndPosition = levelPartStart.Find("EndPosition").position;
        int startingSpawnLevelParts = 5;
        for (var i = 0; i < startingSpawnLevelParts; i++)
        {
            SpawnLevelPart();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(camera.centerPoint, lastEndPosition) < PLAYER_DISTANCE_SPAWN_LEVEL_PART)
        {
            // spawn part 
            SpawnLevelPart();
        }

    }

    private void SpawnLevelPart()
    {

        Transform chosenLevelPart = levelPartList[Random.Range(0, levelPartList.Count)];
        Transform lastLevelTransform = SpawnLevelPart(chosenLevelPart, lastEndPosition);
        lastEndPosition = lastLevelTransform.Find("EndPosition").position;
    }

    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition)
    {

        Transform transform = Instantiate(levelPart, spawnPosition, Quaternion.identity);
        return transform;
    }
}
