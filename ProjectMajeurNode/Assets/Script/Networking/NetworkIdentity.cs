﻿using Project.Utility.Atributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BestHTTP;
using BestHTTP.SocketIO;
namespace Project.Networking
{
    public class NetworkIdentity : MonoBehaviour
    {


        [Header("Helpful value")]
        [SerializeField]
        [GreyOut]
        private string id;

        //public SpriteRenderer sp;

        [SerializeField]
        private MutlipleTargetCamera mutlipleTargetCamera;

        public SpriteRenderer spEnergy;
        public Color m_color;



        [SerializeField]
        [GreyOut]
        private bool isControlling;


        private SocketManager socket;
        // Start is called before the first frame update

        public void Start()
        {
            GameObject go = GameObject.Find("Main Camera");
            MutlipleTargetCamera test = go.GetComponent<MutlipleTargetCamera>();
            if (!test)
                return;
            test.AddTarget(transform);
            //Debug.Log(test);
        }
        void Awake()
        {
            isControlling = false;
        }

        // Update is called once per frame
        public void SetControllerID(string ID)
        {
            id = ID;
            //isControlling = (NetworkClient.ClientID == ID) ? true : false;
        }

        public void SetSocketReference(SocketManager Socket)
        {
            socket = Socket;
        }

        public string GetID()
        {
            return id;
        }

        public bool IsControlling()
        {
            return isControlling;
        }

        public SocketManager GetSocket()
        {
            return socket;
        }
        public void ConvertHtmlTocolor(string color)
        {

            if (ColorUtility.TryParseHtmlString(color, out m_color))
            {
                spEnergy.color = m_color;
            }

        }
    }
}