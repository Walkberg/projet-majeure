﻿using System.Collections;
using System.Collections.Generic;
using Project.InputNetwork;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Globalization;


using BestHTTP;
using BestHTTP.SocketIO;



namespace Project.Networking
{
    public class NetworkClientV2 : MonoBehaviour
    {
        public SocketManager manager;

        [Header("Network client")]
        [SerializeField]
        private Transform networkContainer;

        [SerializeField]
        private GameObject playerPrefab;

        [SerializeField]
        private Transform spawnPoint;

        private Dictionary<string, NetworkIdentity> serverObjects;

        public static string ClientID { get; private set; }

        public GameObject inputField;
        public Text inputField4;
        private Socket Socket;


        public bool isOnline;
        [SerializeField]
        private string url;

        public void Start()
        {
            Init();
            SetupEvents();
            isGameOnline();
        }

        void OnDestroy()
        {
            manager.Close();
        }

        private void Init()
        {

            SocketOptions options = new SocketOptions();
            options.AutoConnect = false;
            //
            manager = new SocketManager(new Uri("http://localhost:3001/socket.io/?type=gameClient"), options);
            serverObjects = new Dictionary<string, NetworkIdentity>();

        }

        private void isGameOnline()
        {
            url = Application.absoluteURL;
            isOnline = (url == "") ? false : true;
            Debug.Log(isOnline);
            string[] tmp = url.Split('?');
            string[] paramTmp = tmp[1].Split('=');
            string param = paramTmp[1];
            // need to create test
            if (isOnline)
                manager.Socket.Emit("JOIN_ROOM", param);
        }

        private void SetupEvents()
        {


            manager.Socket.On(SocketIOEventTypes.Connect, TestOpen);
            manager.Socket.On(SocketIOEventTypes.Disconnect, TestClose);
            manager.Socket.On(SocketIOEventTypes.Error, TestError);

            //manager.Socket.On("register", Regiter);
            manager.Socket.On("spawn", Spawn);
            manager.Socket.On("PLAYER_LEAVE", PlayerLeave);
            manager.Socket.On("UPDATE_BUTTON", UpdateButton);
            manager.Socket.On("MOVE_DETECTED", MoveDetected);
            manager.Socket.On("UPDATE_AXIS", UpdateAxis);
            //manager.Socket.On("close", TestClose);
            manager.Open();
        }


        /* public void Regiter(Socket socket, Packet packet, params object[] args)
         {
             Dictionary<string, object> data = args[0] as Dictionary<string, object>;
             ClientID = data["id"] as string;
         }*/

        public void Spawn(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("Spawn");
            Dictionary<string, object> data = args[0] as Dictionary<string, object>;
            string id = data["id"] as string;
            string color = data["color"] as string;
            SpawnPlayer(id, color);
        }


        public void UpdateButton(Socket socket, Packet packet, params object[] args)
        {
            Dictionary<string, object> data = args[0] as Dictionary<string, object>;
            string id = data["playerId"] as string;
            Dictionary<string, object> button = data["button"] as Dictionary<string, object>;
            string buttonId = button["id"] as string;
            var buttonValue = button["value"];
            NetworkIdentity ni = serverObjects[id];
            ni.GetComponent<InputManager>().SetButtonDown(buttonId, (bool)buttonValue);

        }

        public void MoveDetected(Socket socket, Packet packet, params object[] args)
        {
            Dictionary<string, object> data = args[0] as Dictionary<string, object>;
            string id = data["playerId"] as string;
            string direction = data["direction"] as string;
            NetworkIdentity ni = serverObjects[id];
            ni.GetComponent<InputManager>().SetMovement(direction);
        }

        private void UpdateAxis(Socket socket, Packet packet, params object[] args)
        {
            Dictionary<string, object> data = args[0] as Dictionary<string, object>;
            string id = data["playerId"] as string;
            Dictionary<string, object> axis = data["axis"] as Dictionary<string, object>;
            string axisX = axis["x"] as string;
            string axisY = axis["y"] as string;

            float x;
            float y;

            // flaot convertion different in wevgl and unity editor
            if (isOnline)
            {
                x = float.Parse(axisX);
                y = float.Parse(axisY);
            }
            else
            {
                x = float.Parse(axisX.Replace('.', ','));
                y = float.Parse(axisY.Replace('.', ','));
            }

            NetworkIdentity ni = serverObjects[id];
            ni.GetComponent<InputManager>().SetAxis(x, y);
        }

        public void TestOpen(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("Connect");
            manager.Socket.Emit("test");
        }


        public void TestError(Socket socket, Packet packet, params object[] args)
        {

            Error error = args[0] as Error;

            switch (error.Code)
            {
                case SocketIOErrors.User:
                    Debug.LogWarning("Exception in an event handler!");
                    break;
                case SocketIOErrors.Internal:
                    Debug.LogWarning("Internal error!");
                    break;
                default:
                    Debug.LogWarning("server error!");
                    break;
            }
        }

        public void TestClose(Socket socket, Packet packet, params object[] args)
        {

            Debug.Log("Close");
        }



        public void PlayerLeave(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("Player leave");
            var data = args[0] as Dictionary<string, object>;
            string id = data["playerId"].ToString();
            GameObject go = serverObjects[id].gameObject;
            Destroy(go);
            serverObjects.Remove(id);

        }

        public void SpawnPlayer(string id, string color)
        {
            GameObject go = Instantiate(playerPrefab, networkContainer);
            go.name = string.Format("Player ({0})", id);
            go.transform.position = new Vector2(spawnPoint.position.x, spawnPoint.position.y);
            NetworkIdentity ni = go.GetComponent<NetworkIdentity>();
            ni.SetControllerID(id);
            ni.SetSocketReference(manager);
            ni.ConvertHtmlTocolor(color);
            serverObjects.Add(id, ni);
        }

        public void JoinRoom(string id)
        {
            Debug.Log("Join room");
            string roomId = inputField.GetComponent<Text>().text;
            Debug.Log(roomId);
            manager.Socket.Emit("JOIN_ROOM", roomId);
        }
    }
}





