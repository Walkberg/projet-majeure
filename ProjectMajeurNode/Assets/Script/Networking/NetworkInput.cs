﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Project.Networking
{

    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkInput : MonoBehaviour
    {



        private NetworkIdentity networkIdentity;
        // Start is called before the first frame update
        void Start()
        {
            networkIdentity = GetComponent<NetworkIdentity>();

            if (!networkIdentity.IsControlling())
            {
                enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (networkIdentity.IsControlling())
            {
                // if ()
            }
        }
    }
}
